﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicoinInets.Helper
{
	public class Branch
	{
		public int idch { get; set; }
		public int idf { get; set; }
		public string sname { get; set; }
	}
}