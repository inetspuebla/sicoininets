﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Z.BulkOperations;
using SicoinInets.Models;

namespace SicoinInets.Helper
{
    public class bulk
    {
        public static void inserttemp(List<TmpTask> tmpTask)
        {
            DbSicoinEntities1 db = new DbSicoinEntities1();
            using (var bulking = new BulkOperation(db.Database.Connection))
            {
                db.Dtask();
                db.Database.Connection.Open();
                bulking.DestinationTableName = "TmpTask";
                bulking.BulkInsert(tmpTask);
            }
        }

    }
}