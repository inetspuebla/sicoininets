﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

using SicoinInets.Models;
using SicoinInets.Controllers;
using System.IO;

namespace SicoinInets.Sends
{
    public class Mail
    {
        DbSicoinEntities1 bd = new DbSicoinEntities1();

        public string  enviar (int iduser, int iduser2, int idstatus, int idtasks)
        {
            try
            {
                string header = Headermail(idstatus);
                string body = bodytomail(idstatus, idtasks);
                UserSet usere = bd.UserSets.Find(iduser);
                UserSet userr = bd.UserSets.Find(iduser2);

                EmailSetting email = bd.EmailSettings.Find(1);

                MailMessage mail = new MailMessage();

                SmtpClient smtp = new SmtpClient("smtp.gmail.com, smtp.hotmail.com, smtp.yahoo.com, smtp.inets.com.mx");

                smtp.Host = email.sHost;
                smtp.Port = Convert.ToInt32(email.sPort);
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(email.sUserName, email.sPassword);

                mail.To.Add(userr.sMail + "," + usere.sMail);
                mail.From = new MailAddress(email.sUserName);

                mail.Subject = header;
                mail.Body = body;
                mail.Priority = MailPriority.High;

                smtp.Send(mail);
                mail.Dispose();
                smtp.Dispose();


                return "Correo enviado";

            }
            catch (Exception ex)
            {
                return "Error al enviar correo " + ex;
            }
        }

        private string Headermail(int idstatus)
        {
            string header = "";

            int idstats = idstatus;

            switch (idstats)
            {
                case 1:
                    header = "Alta de una tarea en SICOIN, status: Buzon de tareas  ";
                    break;
                case 3:
                    header = "El usuario marco de enterado en la actividad, status: Sala de espera";
                    break;
                case 4:
                    header = "El usuario marco de enterado en la actividad, status: Abordando";
                    break;
                case 5:
                    header = "La tarea esta atrasado en la entrega, status: Retrasado";
                    break;
                case 6:
                    header = "El usuario envio una tarea terminada al oficial de cumplimiento, status: Enviado";
                    break;
                case 7:
                    header = "El oficial de cumplimiento marco como finalizada la tarea, status: Verificado";
                    break;
                case 8:
                    header = "Cancelación de una tarea por parte del oficial de cumplimiento, status: Cancelado";
                    break;
            }

            return header;
        }
        
        private string bodytomail(int idstatus, int idtask)
        {
            string body = "";
            task task = bd.tasks.Find(idtask);
            UserSet user = bd.UserSets.Find(task.idUserRespon);
            UserSet user2 = bd.UserSets.Find(task.idUser);
            Canceled canceled = bd.Canceleds.Where(x => x.idTask == idtask).FirstOrDefault();
            int idstats = idstatus;

            switch (idstats)
            {
                case 1:
                    body = "SE CREO UNA TAREA NUEVA\n"+
                        "Nombre de la tarea: "+ task.sNameTask + "\n\r" +
                        "Fecha de emisión: " +task.DateEmision;
                    break;

                case 3:
                    body = "EL USUARIO "+ user.sName + " INDICA QUE ESTA ENTERADO DE LA ACTIVIDAD LEVANTADA EN EL SISTEMA Y SE ENCUENTRA EN EL STATUS DE SALA DE ESPERA\n\r" +
                        "Nombre de la tarea: " + task.sNameTask + "\n" +
                        "Fecha de emisión: " + task.DateEmision;
                    break;

                case 4:
                    body = "EL USUARIO " + user.sName + " INDICA QUE ESTA ENTERADO DE LA ACTIVIDAD LEVANTADA EN EL SISTEMA Y SE ENCUENTRA EN EL STATUS DE RETRASADO\n\r" +
                           "Nombre de la tarea: " + task.sNameTask + "\n" +
                           "Fecha de emisión: " + task.DateEmision;
                    break;

                case 5:
                    body = "LA TAREA "+task.sNameTask+ " SE MUEVE A RETRASADO DEBIDO A QUE ESTA CERCA DE LA FECHA DE ENTREGA\n\r" +
                           "Usuario responsable " + user.sName + "\n\r" +
                           "Fecha de emisión: " + task.DateEmision; break;
                case 6:
                    body = "EL USUARIO "+ user.sName + " INDICO HABER TERMINADO LA TAREA, SE ENCUENTRA EN EL STATUS DE ENVIADO\n\r" +
                           "Nombre de la tarea: " + task.sNameTask +"\n" +
                           "Fecha de emisión: " + task.DateEmision;
                    break;
                case 7:
                    body = "EL OFICIAL DE CUMPLIMIENTO " + user2.sName + " MARCO LA TAREA COMO TERMINADA Y FINALIZO LA ACTIVIDAD\n\r" +
                           "Nombre de la tarea: " + task.sNameTask + "\n" +
                           "Fecha de emisión: " + task.DateEmision +"\n"+
                           " Usuario encargado: "+ user.sName;
                    break;
                case 8:
                    body = "EL OFICIAL DE CUMPLIMIENTO " + user2.sName + " CANCELO LA TAREA\n"+
                           " Nombre de la tarea: " + task.sNameTask + "\n" +
                           " Fecha de emisión: " + task.DateEmision + "\n" +
                           " Usuario encargado: " + user.sName + "\n"+
                           "Con la siguiente observación "+canceled.Observation;
                    break;
            }
            return body;
        }
 
        /*
       private List<UserSet> sendsmail(int idstatus)
        {
            string sends = "";
            
            int idstats = idstatus;

            switch (idstats)
            {
                case 1:
                    sends = "";
                    break;
                case 3:
                    sends = "";
                    break;
                case 2:
                    sends = "";
                    break;
                case 4:
                    sends = "";
                    break;
                case 5:
                    sends = "";
                    break;
                case 6:
                    sends = "";
                    break;
                case 7:
                    sends = "";
                    break;
                case 8:
                    sends = "";
                    break;
                case 9:
                    sends = "";
                    break;
            }

            return ;
        }*/
    }
}