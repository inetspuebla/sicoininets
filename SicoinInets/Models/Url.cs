//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SicoinInets.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Url
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Url()
        {
            this.Processes = new HashSet<Process>();
            this.RelationUrls = new HashSet<RelationUrl>();
            this.RelationUrls1 = new HashSet<RelationUrl>();
        }


        public int idUrl { get; set; }

        public Nullable<int> idUrlFather { get; set; }

        [Display(Name = "Nombre del archivo")]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,50}$",
ErrorMessage = "Se permite la captura de  ( ) ´ , : . - /  & # ! ¡ con un máximo  de 50 caracteres.")]
        public string sName { get; set; }

        public string sNameUrl { get; set; }

        [Display(Name = "Tipo de archivo")]
        public string sType { get; set; }

        [Display(Name = "Identificador")]
        public Nullable<int> idTypeoffile { get; set; }

        public Nullable<bool> bActive { get; set; }

        public Nullable<int> idAction { get; set; }

        [Display(Name = "Prioridad")]
        public string sPriority { get; set; }

        public string sIdentity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Process> Processes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RelationUrl> RelationUrls { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RelationUrl> RelationUrls1 { get; set; }
        public virtual Typeoffile Typeoffile { get; set; }
    }
}
