//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SicoinInets.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class typeFulfillment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public typeFulfillment()
        {
            this.tasks = new HashSet<task>();
        }

        public int idTimefulfil { get; set; }

        [Display(Name = "Estatus de cumplimiento")]
        [Required]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,100}$",
   ErrorMessage = "Se permite la captura de  ( ) ´ , : . - /  & # ! ¡ con un máximo  de 100 caracteres.")]
        public string sName { get; set; }

        [Display(Name = "Dia de inicio")]
        public Nullable<int> dayinitial { get; set; }

        [Display(Name = "Días de cumplimiento")]
        [Required]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [Range(0, 31)]
        public Nullable<int> dayfinish { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task> tasks { get; set; }
    }
}
