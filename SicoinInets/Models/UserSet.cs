//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SicoinInets.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class UserSet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserSet()
        {
            this.Processes = new HashSet<Process>();
            this.tasks = new HashSet<task>();
            this.tasks1 = new HashSet<task>();
        }


        public int IdUser { get; set; }

        [Required]
        [Display(Name = "Nombre de usuario")]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,50}$",
ErrorMessage = "Se permite la captura de  ( ) ´ , : . - /  & # ! ¡ con un máximo  de 50 caracteres.")]
        public string sName { get; set; }

        [Required]
        [Display(Name = "Correo electrónico")]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$",
ErrorMessage = "No cumple con las caracteristicas del correo electronico")]
        public string sMail { get; set; }

        [Required]
        [Display(Name = "Perfil")]
        public int IdProfile { get; set; }

        [Required]
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]

        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\=\+\\\,\:\.\-\/\s\&\#\!\¡]{1,50}$", ErrorMessage = "Se permite la captura de numeros y letras")]
        public string sPassword { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Process> Processes { get; set; }
        public virtual ProfileSet ProfileSet { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task> tasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task> tasks1 { get; set; }
    }
}
