//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SicoinInets.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class EmailSetting
    {

        public int idEmailSetting { get; set; }

        [Display(Name = "Host")]
        public string sHost { get; set; }

        [Display(Name = "Puerto")]
        public string sPort { get; set; }

        [Display(Name = "Credencial")]
        public string sUserName { get; set; }

        [Display(Name = "Contraseña")]
        public string sPassword { get; set; }
    }
}
