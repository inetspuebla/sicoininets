//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SicoinInets.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Canceled
    {
        public int idCanceled { get; set; }

        [Display(Name = "Tarea")]
        public Nullable<int> idTask { get; set; }

        [Display(Name = "Observación")]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = " ")]
        [RegularExpression(@"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,200}$",
ErrorMessage = "Se permite la captura de  ( ) ´ , : . - /  & # ! ¡ con un máximo  de 200 caracteres.")]
        public string Observation { get; set; }

        public virtual task task { get; set; }
    }
}
