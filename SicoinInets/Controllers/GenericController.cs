﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using SicoinInets.Models;
using SicoinInets.Utilities;

namespace SicoinInets.Controllers
{
    public class GenericController : Controller
    {
        DbSicoinEntities1 bd = new DbSicoinEntities1();
        
        # region Override
		[NonAction]
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{	
			if (Session["Id"] == null)
			{
				if (HttpContext.Request.IsAjaxRequest())
				{
					Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
					return;
				}
				else
				{
					filterContext.Result = new RedirectToRouteResult(
						new RouteValueDictionary {
								{ "controller", "LogIn"},
								{ "action", "Index"}
						});
				}
				return;
			}

			string originController = filterContext.RouteData.Values["controller"].ToString();
			string originMethod = filterContext.RouteData.Values["action"].ToString();
			List<PermissionSet> LPermission = (List<PermissionSet>)Session["Permissions"];
			var existpermission = LPermission.Where(item => item.sController == originController && item.sMethod == originMethod).FirstOrDefault();
			
			if (existpermission == null)
			{
				
				if (HttpContext.Request.IsAjaxRequest())
				{
					Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
					return;
				}
				else
				{
					TempData["_control"] = originController;
					TempData["_metodo"] = originMethod;
					filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "controller", "Error" }, { "action", "NoPermissions" } });
					return;
				}
			}
			else
			{
				return;
			}

		}
		#endregion

		
	}
}