﻿using SicoinInets.Controllers;
using SicoinInets.Models;
using SicoinInets.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Sicoin.Controllers
{
	public class UrlsSetsController : GenericController
	{
        DbSicoinEntities1 bd = new DbSicoinEntities1();

        //**************************************************VISTA INICIAL Y VISTA POR CARPETA 
        //Inicio de la vista de carpetas
        public ActionResult Index()
		{
			ViewBag.mesage = TempData["Mensaje"];
			Consults consults = new Consults();

			ViewBag.user = bd.UserSets.ToList();

			List<Url> urls = consults.start();

			return View(urls.Where(x => x.idUrlFather == 0));
		}

		//Apertura y transicion de las carpetas
		public ActionResult FileOpen(int idurl)
		{
			ViewBag.mesage = TempData["Mensaje"];
			int iduser = Convert.ToInt16(Session["Id"]);

			string direccion = " ";
			int idsearch = idurl;

			Consults consults = new Consults();

			Url url = bd.Urls.Find(idurl);

			if (idurl == 0)
			{
				Session["urlfather"] = idurl;
				ViewBag.father = idurl;

				List<UserSet> user = bd.UserSets.ToList();//Extrae los usuarios
				ViewBag.User = user.ToList();// retorna a todos los usuarios para elegir quien puede ver 

				List<Typeoffile> typeoffiles = bd.Typeoffiles.ToList();//Muestra los tipo de datos que seran utilizados 
				ViewBag.typeoffiles = typeoffiles.ToList();

				List<Url> urls = consults.start();

				return View(urls.Where(x => x.idUrlFather == 0));

			}
			else
			{
				List<Url> urls = consults.folders(idurl, iduser);//Busca las carpetas que tienen la misma direccion padre

				List<UserSet> user = bd.UserSets.ToList();//Extrae los usuarios
				ViewBag.User = user.ToList();// retorna a todos los usuarios para elegir quien puede ver 

				List<Typeoffile> typeoffiles = bd.Typeoffiles.ToList();//Muestra los tipo de datos que seran utilizados 
				ViewBag.typeoffiles = typeoffiles.ToList();

				Session["urlfather"] = idurl;
				ViewBag.father = idurl;

				List<Url> urlsearch = bd.Urls.ToList();//extrae todos los registros por medio de una consulta generada


				Url idreturn = bd.Urls.Find(idurl);
				ViewBag.page = idreturn.idUrlFather;

				for (int x = 0; x < urlsearch.Count(); x++)
				{
					if (urlsearch[x].idUrl == idsearch)
					{
						direccion = urlsearch[x].sIdentity + " " + urlsearch[x].sName + "/" + direccion;
						idsearch = Convert.ToInt32(urlsearch[x].idUrlFather);
						x = -1;
					}
				}

				Session["sIdentity"] = direccion;// retorna la ruta que se esta creando 

				return View(urls);
			}
		}

		//Funcion de busqueda 
		public ActionResult Search(string text)
		{
         	List<UserSet> user = bd.UserSets.ToList();//Extrae los usuarios

			ViewBag.User = user.ToList();// retorna a todos los usuarios para elegir quien puede ver 
			ViewBag.father = Session["urlfather"];

			int idfather = Convert.ToInt16(Session["urlfather"]);
			int iduser = Convert.ToInt16(Session["Id"]);

			Consults consults = new Consults();
			List<Url> urls = new List<Url>();

			urls = consults.Search(text, iduser, idfather);


			ViewBag.urls = urls.ToList();

			return View();
		}

        //**************************************************FUNCIONES PARA CARPETAS 

        //Funcion para crear un folder nuevo
        [HttpPost]
        public ActionResult CreateFolder(string name, int[] iduser, string priority)
        {
            int urlfather = Convert.ToInt16(Session["urlfather"]);
            string adderss = Session["sIdentity"].ToString();

            if (name != "")
            {
                try
                {
                    Url urlp = bd.Urls.Where(i => i.sName == name && i.bActive == true && i.idUrlFather == urlfather).FirstOrDefault();

                    if (urlp == null)
                    {
                        // Dar de alta una carpeta en la base de datos 
                        Url urlSet = new Url
                        {
                            idUrlFather = Convert.ToInt16(urlfather),
                            sName = name,
                            sNameUrl = name,
                            sType = "Folder",
                            bActive = true,
                            sPriority = priority,
                        };

                        bd.Urls.Add(urlSet);
                        bd.SaveChanges();

                        //Asignar carpeta a un usuario
                        var urlse = bd.Urls.OrderByDescending(x => x.idUrl).First().idUrl;
                        string title = "Creacion de carpeta";

                        Process processes = new Process();
                        if (iduser != null)
                        {
                            for (int x = 0; x < iduser.Length; x++)
                            {
                                processes.IdUrl = urlse;
                                processes.IdUser = iduser[x];
                                processes.sName = title;

                                bd.Processes.Add(processes);
                                bd.SaveChanges();
                            }

                        }

                        //Creacion de carpeta en sus rutas 
                        //+adderss + name

                        var ruta = Server.MapPath("/Raiz/" + adderss + name);

                        if (Directory.Exists(ruta))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(ruta);
                        }

                        TempData["Mensaje"] = new Alert("¡Notificación!", "Folder creado", Alert.Success);
                        return RedirectToAction("FileOpen", new { idurl = urlfather });
                    }
                    else
                    {
                        TempData["Mensaje"] = new Alert("¡Alerta!", "Ya existe carpeta con el mismo nombre", Alert.Danger);
                        return RedirectToAction("FileOpen", new { idurl = urlfather });
                    }
                }
                catch (Exception ex)
                {
                    TempData["Mensaje"] = new Alert("¡Alerta!", "Folder no creado" + ex, Alert.Danger);
                    return RedirectToAction("FileOpen", new { idurl = urlfather });
                }
            }
            else
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "Validar los datos para crear la carpeta", Alert.Danger);
                return RedirectToAction("FileOpen", new { idurl = urlfather });
            }
        }

		//Funcion para editar los permisos
		public ActionResult EditPermiss(int idurl)
		{
			ViewBag.father = Session["urlfather"];

			int iduser = Convert.ToInt16(Session["Id"]);

			Consults consults = new Consults();

			Url urls = bd.Urls.Find(idurl);
			ViewBag.urls = urls;

			List<UserSet> user = consults.userfiles(idurl);//Extrae los usuarios
			ViewBag.user = user.ToList();

			List<UserSet> userpermiso = consults.userSets(idurl);
			ViewBag.permisos = userpermiso.ToList();

			return View();
		}

        //Funcion para editar los permisos
        [HttpPost]
        public ActionResult EditPermiss(int idurl, string sname, int[] iduser)
        {
            var urlfather = Session["urlfather"];

            try
            {
                //Modifica el nombre de usuario
                Url urls = bd.Urls.Find(idurl);

                urls.sName = sname;

                bd.SaveChanges();

                //Asignacion de usuario al archivo
                string title = "Alta de archivo";

                Process processes = new Process();

                for (int x = 0; x < iduser.Length; x++)
                {
                    if (iduser[x] == 0)
                    {

                    }
                    else
                    {
                        processes.IdUrl = idurl;
                        processes.IdUser = iduser[x];
                        processes.sName = title;

                        bd.Processes.Add(processes);
                        bd.SaveChanges();
                    }

                }

                TempData["Mensaje"] = new Alert("¡Notificación!", "Se editaron los permisos para la carpeta o archivo", Alert.Success);

                return RedirectToAction("FileOpen", new { idurl = urlfather });
            }
            catch
            {
                TempData["Mensaje"] = new Alert("¡Notificación!", "Error al modificar los permisos", Alert.Success);
                return View();
            }

        }

		//**************************************************FUNCIONES PARA ARCHIVOS

		//Detalles de los archivos para visualizar que permisos y vinculos tiene generado el archivo al dar de alta 
		public ActionResult DetailFiles(int idurl)
		{
			ViewBag.mesage = TempData["Mensaje"];
			ViewBag.father = Session["urlfather"];

			int iduser = Convert.ToInt16(Session["Id"]);

			Consults consults = new Consults();

			List<Typeoffile> typeoffiles = bd.Typeoffiles.ToList();//Muestra los tipo de datos que seran utilizados 
			ViewBag.typeoffiles = typeoffiles.ToList();

			Url url = bd.Urls.Find(idurl); //Realiza la busqueda de los datos para mostrarlos 
			ViewBag.urls = url;

			List<UserSet> user2 = consults.userSets(idurl);//manda a traer la consulta para traer todos los trabajadores 
			ViewBag.permisos = user2.ToList();

			List<UserSet> user = consults.userfiles(idurl);//llama la consulta donde tiene todos los usuarios con los permisos de visualizar el archivo 
			ViewBag.user = user.ToList();

			List<Url> filesSA = consults.ListFileSA(idurl);//Muestra los archivos que aun no estan compartidos 
			ViewBag.fileSA = filesSA.ToList();

			List<Url> urlfiles = consults.ListFilesSR(idurl);//Muestra los archivos que ya estan vinculados
			ViewBag.listfiles = urlfiles;

			return View(url);
		}

        // Funcion para cargar un archivo nuevo 
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file, string name, int idtypefile, int[] iduser)
        {
            var urlfather = Session["urlfather"];
            string adderss = Session["sIdentity"].ToString();

            string ext = System.IO.Path.GetExtension(file.FileName);

            try
            {

                var archivename = name + DateTime.Now.ToFileTimeUtc();

                //Dar de alta el registro a la base de datos 
                Url urlSet = new Url();

                urlSet.idUrlFather = Convert.ToInt16(urlfather);
                urlSet.sName = name + ext;
                urlSet.sNameUrl = archivename + ext;
                urlSet.sType = ext;
                urlSet.idTypeoffile = idtypefile;
                urlSet.bActive = true;
                urlSet.sPriority = "Baja";

                bd.Urls.Add(urlSet);
                bd.SaveChanges();

                //Asignacion de usuario al archivo
                Url url = bd.Urls.Where(i => i.sNameUrl == urlSet.sNameUrl).FirstOrDefault();
                string title = "Alta de archivo";

                Process processes = new Process();

                for (int x = 0; x < iduser.Length; x++)
                {
                    processes.IdUrl = url.idUrl;
                    processes.IdUser = iduser[x];
                    processes.sName = title;

                    bd.Processes.Add(processes);
                    bd.SaveChanges();
                }

                //Almacenar el archivo en el sistema 
                var path = Path.Combine(Server.MapPath("/Raiz/" + adderss), archivename + ext);

                string ruta = "";
                ruta = Server.MapPath("/Raiz/" + adderss);

                if (Directory.Exists(ruta))
                {

                }
                else
                {
                    Directory.CreateDirectory(ruta);
                }

                file.SaveAs(path);


                TempData["Mensaje"] = new Alert("¡Notificación!", "Archivo cargado", Alert.Success);
                return RedirectToAction("FileOpen", new { idurl = urlfather });
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "Archivo no cargado validar si los datos estan completos" + ex, Alert.Danger);
                return RedirectToAction("FileOpen", new { idurl = urlfather });
            }
        }

		//Actualizacion de los datos y permisos de un archivo 
		[HttpPost]
		public ActionResult UpdateFile(int idurl,string sname, int idtypefile, int[] idusers, int[] idfile)
		{
			int returnid = 0;

			ViewBag.father = Session["urlfather"];
			int iduser = Convert.ToInt16(Session["Id"]);
            
			try
			{
                
                Typeoffile typeoffile = bd.Typeoffiles.Find(idtypefile);

				Url urlp = bd.Urls.Find(idurl);
				returnid = Convert.ToInt32(urlp.idUrlFather);

				//MODIFICACION DE LOS DATOS DE LOS ARCHIVOS 
				Url url = bd.Urls.Find(idurl);
				url.idTypeoffile = idtypefile;
				url.sName = sname;

                bd.SaveChanges();

				//Agregar los permisos a los usuarios
				if (idusers != null)
				{
					Process processes = new Process();
					string title = "Alta de archivo";

					foreach (var item in idusers)
					{
						processes.IdUrl = idurl;
						processes.IdUser = item;
						processes.sName = title;
					}
                    bd.Processes.Add(processes);
                    bd.SaveChanges();
				}

				//Agregar vinculos de los arhivos relacionados
				RelationUrl relation = new RelationUrl();

				if (idfile != null)
				{
					foreach (var item in idfile)
					{
						relation.idurl = idurl;
						relation.idurl2 = item;
					}

                    bd.RelationUrls.Add(relation);
                    bd.SaveChanges();
				}

				TempData["Mensaje"] = new Alert("¡Notificación!", "Archivo editado", Alert.Success);
				return RedirectToAction("FileOpen", new { idurl = returnid });
			}
			catch
			{
				TempData["Mensaje"] = new Alert("¡Alerta!", "No se pudo editar", Alert.Success);
				return RedirectToAction("FileOpen", new { idurl = returnid });
			}
		}

        //Descarga de archivos
        public ActionResult DowFile(int id)
        {
            int ids = id;

            List<Url> urlsearch = bd.Urls.ToList();//extrae todos los registros por medio de una consulta generada
            string direccion = " ";

            for (int x = 0; x < urlsearch.Count(); x++)
            {
                if (urlsearch[x].idUrl == id)
                {
                    if (urlsearch[x].sType == "Folder")
                    {
                        direccion = urlsearch[x].sIdentity + " " + urlsearch[x].sName + "/" + direccion;
                        id = Convert.ToInt32(urlsearch[x].idUrlFather);
                        x = -1;
                    }
                    else
                    {
                        direccion = urlsearch[x].sIdentity + urlsearch[x].sNameUrl;
                        id = Convert.ToInt32(urlsearch[x].idUrlFather);
                        x = -1;
                    }
                }
            }

            Method method = new Method();
            Url url = bd.Urls.Find(ids);

            string ext = System.IO.Path.GetExtension(url.sNameUrl);

            string ruta = Session["sIdentity"].ToString();
            string path = Server.MapPath("~/Raiz/" + direccion);
            string type = method.typedown(ext);

            string filename = path;
            byte[] bytes = System.IO.File.ReadAllBytes(filename);

            return File(bytes, type, url.sNameUrl);

        }

        //FUNCIONES PARA CARPETAS Y ARCHIVOS

        //Funcion para eliminar un usuario
        public ActionResult Deleteuser(int idurl, int iduser)
        {
            var urlfather = Session["urlfather"];

            try
            {
                Process processes = bd.Processes.Where(x => x.IdUser == iduser && x.IdUrl == idurl).FirstOrDefault();
                bd.Processes.Remove(processes);
                bd.SaveChanges();

                TempData["Mensaje"] = new Alert("¡Notificación!", "Se elimino un usuario en permiso de visualizar", Alert.Success);
                return RedirectToAction("FileOpen", new { idurl = urlfather });
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Error al eliminar al usuario de la carpeta", Alert.Danger);
                return View();
            }

        }

        //Funcion para eliminar una relacion de archivo
        public ActionResult Deleterfile(int idurl, int idurl2)
        {
            int returnid = 0;

            try
            {
                Url urlp = bd.Urls.Find(idurl);
                returnid = Convert.ToInt32(urlp.idUrlFather);

                RelationUrl relation = bd.RelationUrls.Where(x => x.idurl == idurl && x.idurl2 == idurl2).FirstOrDefault();

                bd.RelationUrls.Remove(relation);
                bd.SaveChanges();

                TempData["Mensaje"] = new Alert("¡Notificación!", "La vinculacion se elimino", Alert.Success);
                return RedirectToAction("FileOpen", new { idurl = returnid });
            }
            catch
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "No se pudo eliminar la vinculacion ", Alert.Danger);
                return RedirectToAction("FileOpen", new { idurl = returnid });
            }
        }

        //Funcion de eliminar
        public ActionResult Delete(int idUrl)
        {
            var urlfather = Session["urlfather"];
            Url url = bd.Urls.Find(idUrl);
            url.bActive = false;

            bd.SaveChanges();

            TempData["Mensaje"] = new Alert("¡Notificación!", "Este objeto fue eliminado", Alert.Success);
            return RedirectToAction("FileOpen", new { idurl = urlfather });

        }

		//**************************************************FUNCIONES PARA LOS DIAGRAMAS DE CARPETAS 

		// Arbol de carpetas //
		List<Url> url = new List<Url>();

		//Diagrama de carpetas se visualizan que carpetas se tienen en cada una al dar clic ingresaran a sus respectivas funciones 
		public ActionResult Threefile()
		{
            int iduser = Convert.ToInt16(Session["Id"]);

            List<Url> urls = bd.Urls.Where(x => x.idUrlFather == 0).ToList();
			ViewBag.father = urls;

            List<Process> processes = bd.Processes.Where(x => x.IdUser == iduser).ToList();


            foreach (var item in urls)
			{
				if (item.idUrlFather == 0)
				{
					url.Add(new Url
					{
						idUrl = item.idUrl,
						idUrlFather = item.idUrlFather,
						sName = item.sName,
						sNameUrl = item.sNameUrl,
						sType = item.sType,
						sIdentity = item.sIdentity,
						bActive = item.bActive
					});
					Childrens(item.idUrl, item.sIdentity);
				}
			}

			ViewBag.child = url.ToList();

			return View();
		}

		//Funcion para extraer carpetas hijos
		public List<Url> Childrens(int id, string identity)
		{
           List<Url> urls = bd.Urls.ToList();

			foreach (var item in urls)
			{
				if (item.idUrlFather == id)
				{
					if (item.sType == "Folder")
					{
						url.Add(new Url
						{
							idUrl = item.idUrl,
							idUrlFather = item.idUrlFather,
							sName = item.sName,
							sNameUrl = item.sNameUrl,
							sType = item.sType,
							sIdentity = identity,
							bActive = item.bActive
						});
						Childrens(item.idUrl, identity);
					}
				}
			}
			return url;
		}

		//Vista de diagrama de archivos 
		public ActionResult DiagramFiles()
		{
           Consults consults = new Consults();

			List<Typeoffile> typeoffiles = bd.Typeoffiles.ToList();//Muestra los tipo de datos que seran utilizados 
			ViewBag.typeoffiles = typeoffiles.ToList();

			List<Url> folder = consults.ListFolderAC();
			List<Url> folder2 = consults.ListFolderAC();

			ViewBag.Folder = folder.ToList();

			List<Url> Files = new List<Url>();//Consulta para extraer todos los archivos 
			int val = 1;

			foreach (var item in folder2)
			{
				if (item.sIdentity == "CA0" + val && item.sType == "Folder")
				{
					url.Add(new Url
					{
						idUrl = item.idUrl,
						idUrlFather = item.idUrlFather,
						sName = item.sName,
						sNameUrl = item.sNameUrl,
						sType = item.sType,
						idTypeoffile = item.idTypeoffile,
						sIdentity = item.sIdentity,
						bActive = item.bActive
					});

					Childrens2(item.idUrl);
				}
				val = val + 1;
			}

			ViewBag.file = url.ToList();

			return View();
		}

		public List<Url> Childrens2(int id)
		{
			List<Url> urls = bd.Urls.ToList();

			foreach (var item in urls)
			{
				if (item.idUrlFather == id)
				{
					if (item.sType != "Folder")
					{
						url.Add(new Url
						{
							idUrl = item.idUrl,
							idUrlFather = item.idUrlFather,
							sName = item.sName,
							sNameUrl = item.sNameUrl,
							idTypeoffile = item.idTypeoffile,
							sType = item.sType,
							sIdentity = item.sIdentity,
							bActive = item.bActive
						});
						Childrens2(item.idUrl);
					}
				}
			}
			return url;
		}

        public ActionResult DowFile2(int idUrl)
        {
            int ids = idUrl;

            Url url2 = bd.Urls.Find(idUrl);
            int numruta = Convert.ToInt16(url2.idUrlFather);

            List<Url> urlsearch = bd.Urls.ToList();//extrae todos los registros por medio de una consulta generada

            string direccion = " ";

            for (int x = 0; x < urlsearch.Count(); x++)
            {
                if (urlsearch[x].idUrl == numruta)
                {
                    direccion = urlsearch[x].sIdentity + " " + urlsearch[x].sName + "/" + direccion;
                    numruta = Convert.ToInt32(urlsearch[x].idUrlFather);
                    x = -1;
                }
            }

            string dir = direccion;// retorna la ruta que se esta creando 

            for (int x = 0; x < urlsearch.Count(); x++)
            {
                if (urlsearch[x].idUrl == idUrl)
                {
                    if (urlsearch[x].sType == "Folder")
                    {
                        direccion = urlsearch[x].sIdentity + " " + urlsearch[x].sName + "/" + direccion;
                        idUrl = Convert.ToInt32(urlsearch[x].idUrlFather);
                        x = -1;
                    }

                    else
                    {
                        direccion = urlsearch[x].sNameUrl;
                        idUrl = Convert.ToInt32(urlsearch[x].idUrlFather);
                        x = -1;
                    }
                }
            }

            Method method = new Method();
            Url url = bd.Urls.Find(ids);

            string ext = System.IO.Path.GetExtension(url.sNameUrl);

            string ruta = dir;
            string path = Server.MapPath("~/Raiz/" + direccion);
            string type = method.typedown(ext);

            string filename = path;
            byte[] bytes = System.IO.File.ReadAllBytes(filename);

            return File(bytes, type, url.sNameUrl);


        }

	}
}