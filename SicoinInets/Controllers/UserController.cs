﻿using SicoinInets.Controllers;
using SicoinInets.Models;
using SicoinInets.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sicoin.Controllers
{
    public class UserController : GenericController
    {
        private DbSicoinEntities1 db = new DbSicoinEntities1();
        
        //Vista de la pantalla inicial donde realiza la consulta de los usuarios y los muestra en una lista 
        public ActionResult Index()
        {
			ViewBag.mesage = TempData["Mensaje"];
			return View(db.UserSets.ToList());
        }

        //Vista para crear un nuevo usuario
		public ActionResult Create()
		{
            ViewBag.exp = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
			ViewBag.profile = db.ProfileSets.ToList();
			return View();
		}

        //Funcion para crear un usuario una vez llenado el formulario lo recibe y lo carga a la base de datos 
        [HttpPost]
        public ActionResult Create(UserSet user)
        {
            Crypto crypto = new Crypto();
            string newpass = crypto.Encript(user.sPassword);

            user.sPassword = newpass;

            UserSet userset = db.UserSets.Where(i => i.sName == user.sName).FirstOrDefault();
            try
            {
                ViewBag.profile = db.ProfileSets.ToList();

                db.UserSets.Add(user);
                db.SaveChanges();

                TempData["Mensaje"] = new Alert("¡Notificación!", "Usuario ingresado", Alert.Success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.profile = db.ProfileSets.ToList();

                ViewBag.mesage = new Alert("¡Alerta!", "Error al ingresar usuario", Alert.Danger);
                return View(user);
            }
        }

        //Vista para editar un usuaro, con el id te carga un formulario con los datos que fueron dados de alta para editar 
        public ActionResult Edit(int id)
        {
            ViewBag.exp = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

            ViewBag.profile = db.ProfileSets.ToList();
            
            UserSet userSet = db.UserSets.Find(id);
            return View(userSet);

        }

        //Funcionalidad para editar una vez recibido los datos lo edita con el dato que fue dado de alta 
        [HttpPost]
        public ActionResult Edit(UserSet userSet)
        {
            UserSet user2 = db.UserSets.Find(userSet.IdUser);
            
            try
            {
                UserSet user = db.UserSets.Find(userSet.IdUser);

                if (user2.sPassword != userSet.sPassword)
                {
                    Crypto crypto = new Crypto();
                    string newpass = crypto.Encript(userSet.sPassword);
                    user.sPassword = newpass;
                }
                
                user.sName = userSet.sName;
                user.sMail = userSet.sMail;
                user.IdProfile = userSet.IdProfile;


                db.SaveChanges();
                TempData["Mensaje"] = new Alert("¡Notificación!", "Usuario editado", Alert.Success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.profile = db.ProfileSets.ToList();

                ViewBag.mesage = new Alert("¡Alerta!", "Error al editar un usuario", Alert.Danger);
                return View(userSet);
            }

        }

        //Funcion para eliminar un dato que ya fue dado de alta 
        public ActionResult Delete(int id)
        {
            try
            {
                List<Process> processes = db.Processes.Where(x => x.IdUser == id).ToList();
                if (processes != null)
                {
                    foreach (var item in processes)
                    {
                        db.Processes.Remove(item);
                        db.SaveChanges();
                    }
                }

                UserSet user = db.UserSets.Find(id);

                db.UserSets.Remove(user);
                db.SaveChanges();

                TempData["Mensaje"] = new Alert("¡Notificación!", "Usuario eliminado", Alert.Success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "Error al eliminar un usuario", Alert.Danger);
                return RedirectToAction("Index");
            }
        }
    }
}