﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using SicoinInets.Models;
using SicoinInets.Utilities;
using SicoinInets.Controllers;

namespace Sicoin.Controllers
{
    public class ProfileSetsController : GenericController
    {
        private DbSicoinEntities1 db = new DbSicoinEntities1();

        //Vista de pantalla principal de perfiles con un listado de datos ya dados de alta 
        public ActionResult Index()
        {
            ViewBag.mesage = TempData["Mensaje"];
            return View(db.ProfileSets.ToList());
        }

        //Vista de crear un nuevo perfil
        public ActionResult Create()
        {
            return View();
        }

        //Funcionalidad de crear un nuevo perfil esto lo conecta a la base de datos 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProfileSet pfiles)
        {
            ProfileSet pfil = db.ProfileSets.Where(item => item.sName == pfiles.sName).FirstOrDefault();
            if (pfil == null)
            {
                try
                {
                    db.ProfileSets.Add(pfiles);
                    db.SaveChanges();
                    TempData["Mensaje"] = new Alert("¡Notificación!", "Perfil ingresado", Alert.Success);
                    return RedirectToAction("Index");

                }
                catch
                {
                    ViewBag.mesage = new Alert("¡Alerta!", "Error al ingresar el perfil", Alert.Danger);
                    return View();

                }
            }
            else
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Ya existe el perfil", Alert.Danger);
                return View();
            }
        }
		
        //Funcionalidad de eliminar desde la vista index manda la notificacion de eliminar y este hace la eliminacion del dato 
		public ActionResult Delete(int id)
		{
			try
			{
				ProfileRole profrol = db.ProfileRoles.Where(item => item.IdProfile == id).FirstOrDefault();
				if (profrol == null)
				{
					ProfileSet profiles = db.ProfileSets.Find(id);

					db.ProfileSets.Remove(profiles);
					db.SaveChanges();

					TempData["Mensaje"] = new Alert("¡Notificación!", "Perfil eliminado ", Alert.Success);
					return RedirectToAction("Index");
				}
				else
				{
					TempData["Mensaje"] = new Alert("¡Alerta!", "Error el perfil tiene roles asociados", Alert.Danger);
					return RedirectToAction("Index");
				}
			}
			catch
			{
				TempData["Mensaje"] = new Alert("¡Alerta!", "No se elimino el perfil", Alert.Danger);
				return RedirectToAction("Index");
			}
		}

        //Vista de editar donde pedimos el id del dato que fue dado de alta y hacemos una busqueda para mostrarlo en la vista 
        public ActionResult Edit(int id)
        {
            ProfileSet profile = db.ProfileSets.Find(id);
			return View(profile);
        }

        //Funcionalidad de editar donde recibe los datos para cambiarlos al dato que ya esta dado de alta 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProfileSet profile)
       {
			if(ModelState.IsValid)
			{
				try
				{
					db.Entry(profile).State = EntityState.Modified;
					db.SaveChanges();
					TempData["Mensaje"] = new Alert("¡Notificación!", "Perfil editado", Alert.Success);
					return RedirectToAction("Index");
				}
				catch(Exception ex)
				{
					ViewBag.mesage = new Alert("¡Alerta!", "Error al editar el permiso", Alert.Danger);
					return View();
				}
			}
			return View();
        }

        //Vista de seleccionar un rol creado donde cargas la lista de roles que fueron dados de alta 
        public ActionResult assignRole(int id = 0)
        {

            if (id == 0)
            {
                id = Convert.ToInt32(TempData["id"]);
            }
            ViewBag.mesage = TempData["Mensaje"];

            List<RolesSet> list = new List<RolesSet>();
            try
            {
                var ListRol = db.RolesSets.ToList();
                var ListProRol = db.ProfileRoles.Where(item => item.IdProfile == id).ToList();

                foreach (var item2 in ListProRol)
                {
                    foreach (var item in ListRol)
                    {
                        if (item.IdRole == item2.IdRole)
                        {
                            list.Add(item);
                            ListRol.Remove(item);
                            break;
                        }
                    }
                }

                if (ListProRol.Count != 0)
                {
                    ViewBag.RolSelected = list;
                }
                if (ListRol.Count != 0)
                {
                    ViewBag.Rol = ListRol;
                }

                ProfileSet profile = db.ProfileSets.Find(id);
                return View(profile);
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Error al obtener los datos", Alert.Danger);
                return View();
            }
        }

        //Funcionalidad para asignar un rol al perfil, se le asigna el rol y se adjuntan en la tabla 
        [HttpPost]
        public ActionResult AssignRole(ProfileRole Prorol, int[] Roles)
        {

            TempData["id"] = Prorol.IdProfile;
            try
            {
                if (Roles != null)
                {
                    foreach (var item in Roles)
                    {
                        ProfileRole objRol = new ProfileRole();
                        objRol.IdRole = item;
                        objRol.IdProfile = Prorol.IdProfile;
                        ProfileRole Rolper = db.ProfileRoles.Where(Values => Values.IdProfile == Prorol.IdProfile && Values.IdRole == item).FirstOrDefault();
                        if (Rolper == null)
                        {
                            db.ProfileRoles.Add(objRol);
                            db.SaveChanges();
                        }
                        else
                        {
                            return RedirectToAction("AssignRole");
                        }
                    }
                }
                return RedirectToAction("AssignRole");

            }
            catch
            {
                return View("AssignRole");
            }
        }

        //Funcion para des-asignar un rol del perfil que esta dado de alta 
		public ActionResult DeleteRol(int id, int idpro)
        {
            TempData["id"] = idpro;
            try
            {
                ProfileRole rolp = db.ProfileRoles.Where(item => item.IdRole == id && item.IdProfile == idpro).FirstOrDefault();
                //RolesPermissions rolperm = bd.RolesPermissionsSet.Find(id);
                db.ProfileRoles.Remove(rolp);
                db.SaveChanges();
                TempData["Mensaje"] = new Alert("¡Notificacion!", "Rol eliminado", Alert.Success);
                return RedirectToAction("AssignRole");
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Error al eliminar", Alert.Danger);
                return View("AssignRole");
            }

        }

    }
}
