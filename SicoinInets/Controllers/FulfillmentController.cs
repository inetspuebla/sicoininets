﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data.Entity;
using SicoinInets.Models;
using SicoinInets.Utilities;
using SicoinInets.Controllers;

namespace SicoinInets.Controllers
{
    public class FulfillmentController : Controller
    {
        DbSicoinEntities1 db = new DbSicoinEntities1();

        // GET: Fulfillment
        public ActionResult Index()
        {
            ViewBag.mesage = TempData["Mensaje"];

            List<typeFulfillment> type = db.typeFulfillments.ToList();
            return View(type.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(typeFulfillment type)
        {
            typeFulfillment type2 = db.typeFulfillments.Where(x => type.sName == x.sName).FirstOrDefault();

            if (type2 == null)
            {
                try
                {
                    db.typeFulfillments.Add(type);
                    db.SaveChanges();

                    TempData["Mensaje"] = new Alert("¡Notificación!", "Rango de cumplimiento creado", Alert.Success);
                    return RedirectToAction("Index");
                }
                catch
                {
                    ViewBag.mesage = new Alert("¡Alerta!", "Error al generarun nuevo rango ", Alert.Danger);
                    return View();
                }
            }
            else
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Ya existe un rango con este nombre", Alert.Danger);
                return View();
            }

        }

        public ActionResult Edit(int idtype)
        {
            typeFulfillment type2 = db.typeFulfillments.Find(idtype);
            return View(type2);
        }

        [HttpPost]
        public ActionResult Edit(typeFulfillment type)
        {
            DbSicoinEntities1 db = new DbSicoinEntities1();

            typeFulfillment type2 = db.typeFulfillments.Find(type.idTimefulfil);
            
            typeFulfillment tf1 = db.typeFulfillments.Where(x=>x.sName == "Sala de espera").FirstOrDefault();
            typeFulfillment tf2 = db.typeFulfillments.Where(x => x.sName == "Abordando").FirstOrDefault();
            typeFulfillment tf3 = db.typeFulfillments.Where(x => x.sName == "Retrasado").FirstOrDefault();
            try
            {
                if (type2.sName == "Sala de espera")
                {
                    if (tf2.dayfinish < type.dayfinish)
                    {
                        type2.dayfinish = type.dayfinish;
                        db.Entry(type2).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Mensaje"] = new Alert("¡Notificación!", "Datos del rango editado", Alert.Success);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Mensaje"] = new Alert("¡Alerta!", "La fecha debe ser mayor al estado de abordando", Alert.Danger);
                        return RedirectToAction("Index");
                    }
                }

                if (type2.sName == "Abordando")
                {
                    if (tf3.dayfinish < type.dayfinish && tf1.dayfinish > type.dayfinish)
                    {
                        type2.dayfinish = type.dayfinish;
                        db.Entry(type2).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Mensaje"] = new Alert("¡Notificación!", "Datos del rango editado", Alert.Success);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Mensaje"] = new Alert("¡Alerta!", "La fecha debe ser mayor al estado de retrasado y menor que sala de espera", Alert.Danger);
                        return RedirectToAction("Index");
                    }
                }

                if (type2.sName == "Retrasado")
                {
                    if (tf2.dayfinish > type.dayfinish)
                    {
                        type2.dayfinish = type.dayfinish;
                        db.Entry(type2).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Mensaje"] = new Alert("¡Notificación!", "Datos del rango editado", Alert.Success);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Mensaje"] = new Alert("¡Alerta!", "La fecha debe ser menor al estado de abordando", Alert.Danger);
                        return RedirectToAction("Index");
                    }
                }
                TempData["Mensaje"] = new Alert("¡Alerta!", "Cumpla con los campos de edición", Alert.Danger);
                return RedirectToAction("Index");

            }
            catch(Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "Error al editar rango de cumplimiento", Alert.Danger);
                return RedirectToAction("Index");
            }
        }
    }
}