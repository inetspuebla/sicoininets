﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SicoinInets.Models;
using SicoinInets.Utilities;

namespace SicoinInets.Controllers
{
    public class LoginController : Controller
	{
        DbSicoinEntities1 Db = new DbSicoinEntities1();

		// GET: Login
		public ActionResult Index()
		{
			return View();
		}    

		[HttpPost]
		public ActionResult index(string suser, string skey )
		{
            Consults consults = new Consults();

			if (suser == "")
			{
				ViewBag.mesage = new Alert("¡Alerta!", "Escriba el usuario", Alert.Danger);
				return View();
			} else if (skey == "")
			{
				ViewBag.mesage = new Alert("¡Alerta!", "Escriba su contraseña", Alert.Danger);
				return View();
			}
			else
			{
				List<int> permisions = new List<int>();
				List<PermissionSet> permissionSets = new List<PermissionSet>();

				Crypto crypto = new Crypto();

				List<UserSet> userset = Db.UserSets.ToList();


				foreach (var item in userset)
				{
					string passvalidate = crypto.Encript(skey);

					if (item.sName == suser && item.sPassword == passvalidate)
					{
						var user = Db.UserSets.Where(var => var.sName == suser && var.sPassword == item.sPassword).FirstOrDefault();

						var profiles = user.ProfileSet.ProfileRoles.Where(i => i.IdProfile == user.IdProfile).ToList();

						foreach (var p in profiles)
						{
							var permi = Db.RolesPermissons.Where(r => r.IdRole == p.IdRole);

							foreach (var r in permi)
							{
								PermissionSet permiss = Db.PermissionSets.Find(r.IdPermission);
								permissionSets.Add(permiss);
							}
						}

						Session["Id"] = user.IdUser;
						Session["Name"] = user.sName;
						Session["Permissions"] = permissionSets;
						Session.Timeout = 60;

                        consults.Modificltask();

                        return RedirectToAction("Threefile", "UrlsSets");
					}
				}
			}
			ViewBag.mesage = new Alert("¡Alerta!", "La clave o contraseña son incorrectas", Alert.Danger);

			return View();
		}

		public ActionResult LogOut()
		{
			Session.Abandon();
			return RedirectToAction("Index", "Login");
		}
	}
}