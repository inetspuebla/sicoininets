﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SicoinInets.Models;
using SicoinInets.Utilities;
using SicoinInets.Controllers;
using System.Data.Entity;

namespace SicoinInets.Controllers
{
    public class EmailController : GenericController
    {
        DbSicoinEntities1 bd = new DbSicoinEntities1();

        //Patalla principal de vista 
        public ActionResult Index()
        {
            ViewBag.mesage = TempData["Mensaje"];
            List<EmailSetting> emails = bd.EmailSettings.ToList();
            return View(emails);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmailSetting email)
        {
            EmailSetting emaila = bd.EmailSettings.Where(x => x.sHost == email.sHost).FirstOrDefault();
            
            if(emaila == null)
            {
                try
                {
                    Crypto crypto = new Crypto();
                    string newpass = crypto.Encript(email.sPassword);
                    email.sPassword = newpass;

                    bd.EmailSettings.Add(email);
                    bd.SaveChanges();

                    TempData["Mensaje"] = new Alert("¡Notificación!", "SMTP configurado", Alert.Success);
                    return RedirectToAction("Index");
                }
                catch
                {
                    ViewBag.mesage = new Alert("¡Alerta!", "SMTP no fue configurado", Alert.Danger);
                    return View();
                }
            }
            ViewBag.mesage = new Alert("¡Alerta!", "SMTP Ya existe con la host", Alert.Danger);
            return View();
        }

        public ActionResult Edit(int id)
        {
            EmailSetting emailb = bd.EmailSettings.Find(id);

            return View(emailb);
        }

        [HttpPost]
        public ActionResult Edit(EmailSetting email)
        {
            EmailSetting setting = bd.EmailSettings.Find(email.idEmailSetting);
            try
            {
                EmailSetting emailSetting = bd.EmailSettings.Find(email.idEmailSetting);
                emailSetting.sHost = email.sHost;
                emailSetting.sPort = email.sPort;
                emailSetting.sUserName = email.sUserName;
                emailSetting.sPassword = email.sPassword;
                bd.SaveChanges();
                
                TempData["Mensaje"] = new Alert("¡Notificación!", "Configuracion de correo editado", Alert.Success);
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", "Error al editar la configuracion de correo", Alert.Danger);
                return View(setting);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                EmailSetting emailb = bd.EmailSettings.Find(id);

                bd.EmailSettings.Remove(emailb);
                bd.SaveChanges();

                TempData["Mensaje"] = new Alert("¡Notificación!", "Configuración de correo eliminado ", Alert.Success);
                return RedirectToAction("Index");
            }
            catch
            {

                TempData["Mensaje"] = new Alert("¡Alerta!", "Validar que no exista correos asociados a esta configuracion" +
                    "", Alert.Danger);
                return RedirectToAction("Index");
            }
           
        }
    }
}