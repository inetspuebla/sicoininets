﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SicoinInets.Controllers;
using SicoinInets.Models;
using SicoinInets.Utilities;

namespace Sicoin.Controllers
{
    public class PermissionSetsController : GenericController
	{
        private DbSicoinEntities1 db = new DbSicoinEntities1();

        // GET: PermissionSets
        public ActionResult Index()
        {
			ViewBag.mesage = TempData["Mensaje"];
			return View(db.PermissionSets.ToList());
        }

       // GET: PermissionSets/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPermission,sName,sController,sMethod")] PermissionSet permissionSet)
        {
            if (ModelState.IsValid)
            {
				try
				{
					db.PermissionSets.Add(permissionSet);
					db.SaveChanges();
					TempData["Mensaje"] = new Alert("¡Exito!", "Permiso ingresado", Alert.Success);
					return RedirectToAction("Index");
				}
				catch (Exception ex)
				{
					ViewBag.mesage = new Alert("¡Alerta!", "Error al ingresar el permiso", Alert.Danger);
					return View();
				}

			}
			else
			{
				ViewBag.mesage = new Alert("¡Alerta!", "Ya existe el permiso", Alert.Danger);
				return View();
			}
			
        }

        // GET: PermissionSets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PermissionSet permissionSet = db.PermissionSets.Find(id);
            if (permissionSet == null)
            {
                return HttpNotFound();
            }
            return View(permissionSet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPermission,sName,sController,sMethod")] PermissionSet permissionSet)
        {
            if (ModelState.IsValid)
            {
				try
				{
					db.Entry(permissionSet).State = EntityState.Modified;
					db.SaveChanges();
					TempData["Mensaje"] = new Alert("¡Exito!", "Permiso editado", Alert.Success);
					return RedirectToAction("Index");
				}catch(Exception ex)
				{
					ViewBag.mesage = new Alert("¡Alerta!", "Error al editar el permiso", Alert.Danger);
					return View();
				}

			}
            return View(permissionSet);
        }

        // GET: PermissionSets/Delete/5
        public ActionResult Delete(int? id)
        {
			PermissionSet permissionSet = db.PermissionSets.Find(id);
			db.PermissionSets.Remove(permissionSet);
			db.SaveChanges();
			return RedirectToAction("Index");
		}
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

	}
}
