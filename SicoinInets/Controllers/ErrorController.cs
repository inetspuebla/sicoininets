﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SicoinInets.Controllers
{
    public class ErrorController : Controller
    {
		public ActionResult NoPermissions()
		{
			ViewBag.control = TempData["_control"];
			ViewBag.metodo = TempData["_metodo"];
			return View();
		}
	}
}