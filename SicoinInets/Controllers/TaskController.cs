﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SicoinInets.Models;
using SicoinInets.Utilities;
using System.Data.Entity;
using SicoinInets.Sends;
using SicoinInets.Bussines;

using System.IO;


namespace SicoinInets.Controllers
{
    public class TaskController : Controller
    {
        DbSicoinEntities1 bd = new DbSicoinEntities1();
        private string folder = "Temp";

        // Pantalla principal para mostrar las listas de los status que tiene el sistema 3
        public ActionResult Monitor()
        {
            Consults consults = new Consults();
            int iduser = Convert.ToInt16(Session["Id"]);
            ViewBag.iduser = Convert.ToInt16(Session["Id"]);
            List<typeFulfillment> stats = bd.typeFulfillments.ToList();

           foreach(var item in stats)
            {
                foreach(var item2 in consults.countstask(iduser))
                {
                    if (item.sName == "Buzón de tareas" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts = (item2.counter > 0) ? item2.counter :0;
                    }
                    if (item.sName == "Sala de espera" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts2 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                    if (item.sName == "Abordando" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts3 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                    if (item.sName == "Retrasado" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts4 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                    if (item.sName == "Enviado" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts5 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                    if (item.sName == "Verificado" && item.idTimefulfil == item2.idtask )
                    {
                        ViewBag.counts6 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                    if (item.sName == "Cancelado" && item.idTimefulfil == item2.idtask)
                    {
                        ViewBag.counts7 = (item2.counter > 0) ? item2.counter : 0; ;
                    }
                }
            }
           
            ViewBag.mesage = TempData["Mensaje"];
            return View();
        }

        //Al dar clic en cada boton te muestra los status que se quiere consultar solo mostrara las tareas que tiene asignada 
        public ActionResult Lstatus(int range, int id)
        {
            Consults consults = new Consults();
            DbSicoinEntities1 db = new DbSicoinEntities1();

            int iduser = Convert.ToInt16(Session["Id"]);
            ViewBag.iduser = Convert.ToInt16(Session["Id"]);
            ViewBag.range = range;
            List<task> tasks = consults.tasklist(iduser, range);

            return PartialView("Lstatus",tasks);
        }
        
        //Vista de la pantalla de crear tareas 
        public ActionResult Create()
        {
            int idus = Convert.ToInt16(Session["Id"]);

            ViewBag.iduser = Convert.ToInt16(Session["Id"]);
            ViewBag.userp = bd.UserSets.ToList();
            ViewBag.user = bd.UserSets.Where(x => x.IdUser != idus);
            ViewBag.timerdate = DateTime.UtcNow;
            return View();
        }

        //Funcion de crear una tarea y asignarlo a un usuario
        [HttpPost]
        public ActionResult Create(task task)
        {
            try
            {
                Mail mail = new Mail();

                ViewBag.user = bd.UserSets.ToList();
                ViewBag.userp = bd.UserSets.ToList();

                task.DateEmision = DateTime.Now.Date;
                task.idTimefulfil = 1;
                
                bd.tasks.Add(task);
                bd.SaveChanges();

                task task2 = bd.tasks.Where(x => x.sNameTask == task.sNameTask).FirstOrDefault();

                string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon),Convert.ToInt32(task.idTimefulfil),task2.Idtask);

                TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea agregada "+ message , Alert.Success);
                return RedirectToAction("Monitor");
            }
            catch
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", " Valide que los datos estan completos", Alert.Danger);
                return RedirectToAction("Monitor");
            }
        }
       
        //RANGOS DE CUMPLIMIENTO CADA FUNCION QUE DEBE REALIZAR EL SISTEMA CON EL MONITOR-------------------------------------------
        
        //Funcion de aceptar la tarea y validar de que rango de fechas se esta ingresando el rango de cumpimiento
        public ActionResult AceptTask(int idtask)
        {
            try
            {
                Mail mail = new Mail();
                task task = bd.tasks.Find(idtask);
                DateTime now = DateTime.UtcNow;

                TimeSpan ts = Convert.ToDateTime(task.dDelivery) - now;

                typeFulfillment type = bd.typeFulfillments.Where(x => x.dayfinish > 0 && x.dayfinish < Math.Abs(ts.Days) ).FirstOrDefault();

                if (type == null)
                {
                    task.idTimefulfil = 5;
                    bd.SaveChanges();

                    string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil),task.Idtask);

                    TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada a retrasado", Alert.Success);

                    return RedirectToAction("Monitor");
                }else
                {
                    if (type.idTimefulfil == 3)
                    {
                        task.idTimefulfil = 3;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada en sala de espera", Alert.Success);

                        return RedirectToAction("Monitor");

                    }
                    else
                    if (type.idTimefulfil == 4)
                    {
                        task.idTimefulfil = 3;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada en sala de espera", Alert.Success);

                        return RedirectToAction("Monitor");

                    }
                    else if (type.idTimefulfil == 5)
                    {
                        task.idTimefulfil = 4;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), 2, task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada a abordando", Alert.Success);
                        return RedirectToAction("Monitor");
                    }
                }

                TempData["Mensaje"] = new Alert("¡Alerta!", " Surgio un error al crear la tarea", Alert.Danger);
                return RedirectToAction("Monitor");
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", " Valide que los datos estan completos", Alert.Danger);
                return RedirectToAction("Monitor");
            }
        }
        
        //Funcion para enviar la tarea
        public ActionResult pEnviar(int idtask)
        {
            try
            {
                task task = bd.tasks.Find(idtask);
                Mail mail = new Mail();
                
                task.idTimefulfil = 6;
                bd.SaveChanges();

                string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea enviada", Alert.Success);
                return RedirectToAction("Monitor");

            }catch(Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", " Hubo un error en generar el envio de la tarea reinicie la sesion", Alert.Success);
                return RedirectToAction("Monitor");
            }

        }

        //Funcion para finalizar la tarea
        public ActionResult Finalitytask(int idtask)
        {
            try
            {
                task task = bd.tasks.Find(idtask);
                Mail mail = new Mail();

                task.idTimefulfil = 7;
                bd.SaveChanges();

                string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea terminada", Alert.Success);
                return RedirectToAction("Monitor");

            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", " Hubo un error al finalizar la tarea", Alert.Success);
                return RedirectToAction("Monitor");
            }

        }

        //Funcion para indicar que esta incompleta y regresarlos a enviado
        public ActionResult IncompleteTask(int idtask)
        {
            try
            {
                task task = bd.tasks.Find(idtask);
                Mail mail = new Mail();

                DateTime now = DateTime.UtcNow;

                TimeSpan ts = Convert.ToDateTime(task.dDelivery) - now;

                typeFulfillment type = bd.typeFulfillments.Where(x => x.dayfinish > 0 && x.dayfinish < Math.Abs(ts.Days)).FirstOrDefault();

                if (type == null)
                {
                    task.idTimefulfil = 5;
                    bd.SaveChanges();

                    string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                    TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada a retrasado", Alert.Success);

                    return RedirectToAction("Monitor");
                }
                else
                {
                    if (type.idTimefulfil == 3)
                    {
                        task.idTimefulfil = 3;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada en sala de espera", Alert.Success);

                        return RedirectToAction("Monitor");

                    }
                    else
                    if (type.idTimefulfil == 4)
                    {
                        task.idTimefulfil = 3;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada en sala de espera", Alert.Success);

                        return RedirectToAction("Monitor");

                    }
                    else if (type.idTimefulfil == 5)
                    {
                        task.idTimefulfil = 4;
                        bd.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), 2, task.Idtask);

                        TempData["Mensaje"] = new Alert("¡Notificación!", " Tarea aceptada y agregada a abordando", Alert.Success);
                        return RedirectToAction("Monitor");
                    }
                }

                return RedirectToAction("Monitor");

            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", " Hubo un error al retornar la tarea", Alert.Success);
                return RedirectToAction("Monitor");
            }

        }
        
        //Vista para mostrar como cancelar una tarea que se esta realizando 
        public ActionResult Canceledtask(int id)
        {
            task task = new task();
            
            task = bd.tasks.Find(id);
            ViewBag.task = task;
            
            return PartialView("Canceledtask");
        }

        //Funcion para realizar la cancelacion, no solo eso tambien guarda el regisgtro de la tarea cancelada  
        [HttpPost]
        public ActionResult CanceledTask(Canceled canceled, int idTask)
        {
            try
            {
                task task = bd.tasks.Find(idTask);

                Mail mail = new Mail();

                task.idTimefulfil = 8;

                bd.Canceleds.Add(canceled);

                bd.SaveChanges();

                string message = mail.enviar(Convert.ToInt32(task.idUser), Convert.ToInt32(task.idUserRespon), Convert.ToInt32(task.idTimefulfil), task.Idtask);

                TempData["Mensaje"] = new Alert("¡Notificación!", "Tarea cancelada", Alert.Success);
                return RedirectToAction("Monitor");
            }
            catch
            {
                ViewBag.mesage = new Alert("¡Alerta!", " Valide que los datos estan completos", Alert.Danger);
                return RedirectToAction("Monitor");
            }
        }

        //Vista para detalles de los cancelados
        public ActionResult detailcanceled (int idtask)
        {
            task task = bd.tasks.Find(idtask);
            Canceled canceled = bd.Canceleds.Where(x => x.idTask == idtask).FirstOrDefault();
            ViewBag.cancel = canceled.Observation;
            return View(task);
        }

        //CARGA DE ARCHIVOS DEL SISTEMA 

        public ActionResult UploadTask()
        {
            ViewBag.iduser = Convert.ToInt16(Session["Id"]);
            TempData["iduser"] = Session["Id"];
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(string val)
        {
            int iduser =Convert.ToInt32( TempData["iduser"]);
            var messageDelete = " Información del archivo de tareas ha sido cargada.";

            bool answersave = false; string msn = "";
            FileStream stream;
            var namefilesource = Data_.GetDateNow();
            Alert _notify = new Alert();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                string namefile = file.FileName;

                if (file != null && file.ContentLength > 0)
                {
                    //Extrae la extension
                    var msncheckextension = File_.CheckExtension(file.FileName, folder) == false ? " Se aceptan archivos con extension .xlsx ó .xls." : "";

                    (answersave, msn, namefilesource) = File_.SaveFile(file, folder, namefilesource);

                    //Lectura del archivo
                    if (answersave == false)
                    {
                        _notify = new Alert("¡Error!",
                                            " Ocurrio un error al intentar leer el archivo. " +
                                            "Intente nuevamente o llame al administrador. " + msn,
                                            Alert.Danger);
                        return Json(new { answer = 1, _notify });

                    }
                    ResponseWrapper errors = new ResponseWrapper();

                    //Busqueda del archivo
                    if (File_.ExistFile(namefilesource, folder) != true)
                    {
                        _notify = new Alert("¡Alerta!",
                                           " Archivo no encontrado.",
                                           Alert.Danger);
                        return Json(new { answer = 1, _notify });
                    }

                     UFtask filexcel = new UFtask();

                    (msn, stream) = File_.OpenFile(namefilesource);

                    if (msn != "")
                    {
                        errors.AddError(msn);
                        var _msn = errors.ToString();
                        return Json(new { answer = 1, _msn, cont = errors.Errors.Count });
                    }

                    //Comparar filas y columnas y definir nombre de columnas
                    (int numberError, string msndata) = filexcel.CompareNumberColumnsFileWithStructHeader(stream, UFtask.Header);
                    if (numberError == 1)//
                    {
                        File_.CloseFile(stream);
                        errors.AddError(
                            Data_.GetToStringList(UFtask.Header) +
                            " definidas no coincide con las del archivo " +
                            Data_.GetToStringList(filexcel.GetHeaderFile())
                        );
                        var _msn = errors.ToString();
                        return Json(new { answer = 1, _msn, cont = errors.Errors.Count });
                    }

                    if (numberError == -1)
                    {
                        File_.CloseFile(stream);
                        errors.AddError(
                            "¡Alerta! " + msndata
                        );
                        var _msn = errors.ToString();
                        return Json(new { answer = 1, _msn, cont = errors.Errors.Count });
                    }

                    // COMPARE NAME FILE´s COLUMNS AND NAME DEFINE COLUMNS
                    (bool answer, List<string> error) = filexcel.CompareColumnsFileWithStructHeader(UFtask.Header);
                    if (answer == false) // struct header different
                    {
                        errors.AppendRangeErrors(error);
                    }
                    // LOAD DATA AND CHECK DATA
                    (List<TmpTask> ltask, List<String> errors_) = filexcel.Loadftask();
                    if (errors_.Count() > 0)
                    {
                        File_.CloseFile(stream);
                        errors.AppendRangeErrors(errors_);
                        var _msn = errors.ToString();
                        return Json(new { answer = 1, _msn, cont = errors.Errors.Count });
                    }
                    //UPDATE INFORMATION

                    error = bTask.Savetask(ltask, iduser);
                    
                    Alert _notify_ = new Alert();
                    string _msn_ = "";
                    File_.CloseFile(stream);// MUST CLOSE FILE OR DELETE

                    System.IO.File.Delete(namefilesource);

                    if (error.Count <= 0)
                    {
                        _notify_ = new Alert("¡Exito!", messageDelete, Alert.Success);
                        return Json(new { answer = 1, _notify = _notify_, _msn = _msn_ });
                    }

                    errors.AppendRangeErrors(error);
                    _msn_ = errors.ToString();
                    return Json(new { answer = 1, _notify = _notify_, _msn = _msn_, cont = errors.Errors.Count });
                }
            }
            _notify = new Alert("¡Error!", " Cargue archivo.", Alert.Danger);
            return Json(new { answer = 1, _notify });
        }

    }
}