﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SicoinInets.Utilities;
using SicoinInets.Models;
using SicoinInets.Controllers;

namespace Sicoin.Controllers
{
    public class RolesSetsController : GenericController
    {
        private DbSicoinEntities1 db = new DbSicoinEntities1();
        
        //Vista de la pantalla inicial donde carga la lista de los datos que ya fueron dados de alta 
		public ActionResult Index()
        {
            ViewBag.mesage = TempData["Mensaje"];

            return View(db.RolesSets.ToList());
        }

        //Vista de la pantalla crear 
        public ActionResult Create()
        {
            return View();
        }
		
		//Funcion para crear el rol y retornar al index con notificacion de que fue dado de alta o no
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(RolesSet rol)
		{
			try
			{
				db.RolesSets.Add(rol);
				db.SaveChanges();
				TempData["Mensaje"] = new Alert("¡Notificación!", "Rol ingresado", Alert.Success);
				return RedirectToAction("Index");
			}
			catch
			{
				ViewBag.mesage = new Alert("¡Alerta!", "Error al ingresar valide los datos", Alert.Danger);
				return View();
			}
		}

        //Funcion para eliminar un rol, en el index aparece una notificacion de eliminar y esta funcion realiza la accion solo recarga la vista 
        public ActionResult Delete(int id)
        {
            try
            {
                RolesPermisson rolper = db.RolesPermissons.Where(item => item.IdRole == id).FirstOrDefault();
                if (rolper == null)
                {
                    RolesSet rol = db.RolesSets.Find(id);
                    db.RolesSets.Remove(rol);
                    db.SaveChanges();
                    TempData["Mensaje"] = new Alert("¡Notificación!", "Rol eliminado", Alert.Success);
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Mensaje"] = new Alert("¡Alerta!", "Error el rol tiene permisos asociados", Alert.Danger);
                    return RedirectToAction("Index");
                }

            }
            catch
            {
                TempData["Mensaje"] = new Alert("¡Alerta!", "Error al eliminar", Alert.Danger);
                return RedirectToAction("Index");
            }
        }

        //Vista para editar un rol por medio del id realiza la consulta y extrae el dato que coincida y lo muestra en la vista 
        public ActionResult Edit(int id)
        {
            RolesSet rol = db.RolesSets.Find(id);
            return View(rol);
        }

        //Funcion de editar al momento de hacer un cambio en el dato, manda la info y modifica el rol que ya fue dado de alta 
		[HttpPost]
        public ActionResult Edit(RolesSet rol)
        {
			if(ModelState.IsValid)
			{
				try
				{
					db.Entry(rol).State = EntityState.Modified;
					db.SaveChanges();
					TempData["Mensaje"] = new Alert("¡Notificación!", "Rol editado", Alert.Success);
					return RedirectToAction("Index");
				}
				catch(Exception ex)
				{
					ViewBag.mesage = new Alert("¡Alerta!", "Error al editar el permiso", Alert.Danger);
					return View();
				}
			}
			return View();
		}

		//Vista para asignar permisos que estan generados en la pagina 
		public ActionResult AddPermissions(int id = 0)
		{
			if (id == 0)
			{
				id = Convert.ToInt32(TempData["id"]);
			}
			ViewBag.mesage = TempData["Mensaje"];

			List<PermissionSet> list = new List<PermissionSet>();
			try
			{
				var ListPer = db.PermissionSets.ToList();
				var ListRolPer = db.RolesPermissons.Where(item => item.IdRole == id).ToList();

				foreach (var item2 in ListRolPer)
				{
					foreach (var item in ListPer)
					{
						if (item.IdPermission == item2.IdPermission)
						{
							list.Add(item);
							ListPer.Remove(item);
							break;
						}
					}
				}

				if (ListRolPer.Count != 0)
				{
					ViewBag.PerSelected = list;
				}
				if (ListPer.Count != 0)
				{
					ViewBag.Per = ListPer;
				}

				RolesSet roles = db.RolesSets.Find(id);
				return View(roles);
			}
			catch
			{
				ViewBag.mesage = new Alert("¡Alerta!", "Error al obtener los datos", Alert.Danger);
				return View();
			}
		}

		//Funcion para ir asignando el permiso a cada rol recibe ambos id y los asigna uno por uno 
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddPermissions(RolesPermisson rolper, int[] Permissions)
		{
			TempData["id"] = rolper.IdRole;
			if (!ModelState.IsValid)
			{
				return View();
			}
			else
			{
				try
				{
					if (Permissions != null)
					{
						foreach (var item in Permissions)
						{
							RolesPermisson objRol = new RolesPermisson();
							objRol.IdPermission = item;
							objRol.IdRole = rolper.IdRole;
							RolesPermisson Rolper = db.RolesPermissons.Where(Values => Values.IdRole == rolper.IdRole && Values.IdPermission == item).FirstOrDefault();
							if (Rolper == null)
							{
								db.RolesPermissons.Add(objRol);
								db.SaveChanges();
							}
							else
							{
								return RedirectToAction("AddPermissions");
							}
						}
					}
					return RedirectToAction("AddPermissions");

				}
				catch
				{
					return View();
				}
			}

		}

        //Funcion para des-asignar los permisos del rol que fueron asignados 
		public ActionResult DeletePermition(int id, int idrol)
		{
			TempData["id"] = idrol;
			try
			{
				RolesPermisson rolp = db.RolesPermissons.Where(item => item.IdPermission == id && item.IdRole == idrol).FirstOrDefault();
				//RolesPermissions rolperm = bd.RolesPermissionsSet.Find(id);
				db.RolesPermissons.Remove(rolp);
				db.SaveChanges();
				TempData["Mensaje"] = new Alert("¡Notificación!", "Permiso eliminado", Alert.Success);
				return RedirectToAction("AddPermissions", id);
			}
			catch
			{

				ViewBag.mesage = new Alert("¡Alerta!", "Error al eliminar", Alert.Danger);
				return View("AddPermissions");
			}

		}


	}
}
