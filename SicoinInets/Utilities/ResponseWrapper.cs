﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicoinInets.Utilities
{
    public class ResponseWrapper
    {
        #region Props
        public int Status { get; set; }

        public string Message { get; set; }

        public List<string> Errors { get; }
        #endregion


        public ResponseWrapper()
        {
            Errors = new List<string>(0);

        }


        #region Methods
        public void AddError(string error)
        {
            Errors.Add(error);
        }
        public void AppendRangeErrors(List<string> errors)
        {
            Errors.AddRange(errors);
        }


        public override string ToString()
        {
            string result = "";
            foreach (var item in Errors)
            {
                result += item + "\n";
            }
            return result;
        }

        #endregion
    }
}