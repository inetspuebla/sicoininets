﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicoinInets.Utilities
{
    public class DateTimeZone_
    {
        public static DateTime Now()
        {
            TimeZoneInfo cst2 = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            var convertedTime2 = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, cst2);
            return convertedTime2;
        }
    }
}