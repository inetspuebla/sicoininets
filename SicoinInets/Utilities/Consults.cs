﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SicoinInets.Models;
using SicoinInets.Helper;
using SicoinInets.Sends;

namespace SicoinInets.Utilities
{
    public class Consults
    {
        DbSicoinEntities1 db = new DbSicoinEntities1();

        //Consulta inicial de todas las carpetas raiz
        public List<Url> start()
        {
            List<Url> urls = new List<Url>();

            var url = (from u in db.Urls
                       join p in db.Processes on u.idUrl equals p.IdUrl
                       join us in db.UserSets on p.IdUser equals us.IdUser
                       where u.idUrl == p.IdUrl &&
                           us.IdUser == p.IdUser
                       select new
                       {
                           u.idUrl,
                           u.idUrlFather,
                           u.sName,
                           u.sNameUrl,
                           u.sType,
                           u.sIdentity,
                           u.bActive
                       });

            foreach (var item in url)
            {
                urls.Add(new Url()
                {
                    idUrl = item.idUrl,
                    idUrlFather = item.idUrlFather,
                    sName = item.sName,
                    sNameUrl = item.sNameUrl,
                    sType = item.sType,
                    sIdentity = item.sIdentity,
                    bActive = item.bActive
                });
            }

            return urls;
        }

        //Consulta para extraer los folders de la carpeta y validando el usuario
        public List<Url> folders(int idurl, int iduser)
        {
            List<Url> urls = new List<Url>();

            var url = (from u in db.Urls
                       join p in db.Processes on u.idUrl equals p.IdUrl
                       join us in db.UserSets on p.IdUser equals us.IdUser
                       where u.idUrlFather == idurl &&
                           u.idUrl == p.IdUrl &&
                           us.IdUser == p.IdUser &&
                           p.IdUser == iduser
                       select new
                       {
                           u.idUrl,
                           u.idUrlFather,
                           u.sName,
                           u.sNameUrl,
                           u.sType,
                           u.sIdentity,
                           u.bActive,
                           u.sPriority
                       });

            foreach (var item in url)
            {
                urls.Add(new Url()
                {
                    idUrl = item.idUrl,
                    idUrlFather = item.idUrlFather,
                    sName = item.sName,
                    sNameUrl = item.sNameUrl,
                    sType = item.sType,
                    sIdentity = item.sIdentity,
                    bActive = item.bActive,
                    sPriority = item.sPriority
                });
            }

            return urls;
        }

        //Consulta los usuarios que se les asigno a la carpeta
        public List<UserSet> userSets(int idurl)
        {

            List<UserSet> users = new List<UserSet>();

            var user = (from us in db.UserSets
                        join p in db.Processes on us.IdUser equals p.IdUser
                        join u in db.Urls on p.IdUrl equals u.idUrl
                        where p.IdUrl == idurl
                        select new
                        {
                            us.IdUser,
                            us.sName
                        });
            foreach (var item in user)
            {
                users.Add(new UserSet
                {
                    IdUser = item.IdUser,
                    sName = item.sName
                });
            }

            return users;
        }

        //Consulta los usuarios que no han sido asignados a una carpeta
        public List<UserSet> userfiles(int idurl)
        {

            List<UserSet> userfile = new List<UserSet>();
            List<UserSet> users = db.UserSets.ToList();
            List<UserSet> noexist = new List<UserSet>();
            List<UserSet> exist = new List<UserSet>();

            var proce = (from u in db.UserSets
                         from p in db.Processes
                         where p.IdUrl == idurl
                         && p.IdUser == u.IdUser

                         select new
                         {
                             u.IdUser,
                             u.sName
                         });

            foreach (var item in proce)
            {
                userfile.Add(new UserSet { IdUser = item.IdUser, sName = item.sName });
            }

            exist = (from u in users
                     where (from us in userfile select us.IdUser).Contains(u.IdUser)
                     select u).Distinct().ToList();

            noexist = (from u in users
                       where !(from e in exist select e.IdUser).Contains(u.IdUser)
                       select u).ToList();

            return noexist;
        }

        //Realiza la busqueda de archivos o carpetas 
        public List<Url> Search(string text, int iduser, int idfather)
        {
            List<Url> urls = new List<Url>();

            var url = (from u in db.Urls
                       from p in db.Processes
                       from us in db.UserSets
                       where p.IdUser == us.IdUser &&
                       p.IdUrl == u.idUrl &&
                       us.IdUser == iduser &&
                       u.sName.Contains(text)
                       select new
                       {
                           u.idUrl,
                           u.sName,
                           u.sNameUrl,
                           u.sType,
                           u.sIdentity,
                           u.bActive
                       });

            foreach (var item in url)
            {
                urls.Add(new Url
                {
                    idUrl = item.idUrl,
                    sName = item.sName,
                    sNameUrl = item.sNameUrl,
                    sType = item.sType,
                    sIdentity = item.sIdentity,
                    bActive = item.bActive
                });
            }

            return urls;
        }

        //Extrae las carpetas administracion de controla para mostrar al inicio 
        public List<Url> ListFolderAC()
        {
            List<Url> urls = new List<Url>();

            var folder = (from u in db.Urls
                          where u.sIdentity.Contains("CA")
                          select new
                          {
                              u.idUrl,
                              u.idUrlFather,
                              u.sName,
                              u.sType,
                              u.sIdentity,
                              u.bActive,
                              u.sPriority,
                          });
            foreach (var item in folder)
            {
                urls.Add(new Url
                {
                    idUrl = item.idUrl,
                    idUrlFather = item.idUrlFather,
                    sName = item.sName,
                    sType = item.sType,
                    sIdentity = item.sIdentity,
                    bActive = item.bActive,
                    sPriority = item.sPriority
                });
            }

            return urls;
        }

        //Lista de archivos sin asignar 
        public List<Url> ListFileSA(int idurl)
        {
            List<Url> url = db.Urls.ToList();
            List<Url> urls = new List<Url>();
            List<Url> exist = new List<Url>();
            List<Url> noexist = new List<Url>();

            var file = (from u in db.Urls
                        join ru in db.RelationUrls on u.idUrl equals ru.idurl
                        into urlrelation
                        from ure in urlrelation.DefaultIfEmpty()
                        where u.idUrl != idurl
                        && u.sType != "Folder"
                        select new
                        {
                            u.idUrl,
                            u.sName
                        });

            foreach (var item in file)
            {
                urls.Add(new Url
                {
                    idUrl = item.idUrl,
                    sName = item.sName
                });
            }
            return urls;
        }

        //Mostrar archivos que ya fueron dados de alta
        public List<Url> ListFilesSR(int idurl)
        {
            List<Url> urls = new List<Url>();

            var files = (from ru in db.RelationUrls
                         join u in db.Urls on ru.idurl2 equals u.idUrl
                         where ru.idurl == idurl
                         select new
                         {
                             ru.idurl2,
                             u.idUrlFather,
                             u.sName,
                             u.idAction,
                         });

            foreach (var item in files)
            {
                urls.Add(new Url
                {
                    idUrl = Convert.ToInt32(item.idurl2),
                    idUrlFather = item.idUrlFather,
                    sName = item.sName,
                    idAction = item.idAction
                });
            }

            return urls;
        }

        //Mostrar el listado de tareas del usuario que tiene asignado  
        public List<task> tasklist(int iduser, int status)
        {
            string type = null;
            if (status == 1)
            {type = "Buzón de tareas";
            }
            if (status == 2)
            {type = "Sala de espera";
            }
            if (status == 3)
            {type = "Abordando";
            }
            if (status == 4)
            {type = "Retrasado";
            }
            if (status == 5)
            {type = "Enviado";
            }
            if (status == 6)
            {
                type = "Verificado";
            }
            if (status == 7)
            {type = "Cancelado";
            }

            List<task> ltask = new List<task>();
            typeFulfillment vstatus = db.typeFulfillments.Where(x=> x.sName == type).FirstOrDefault();

            ltask = db.tasks.Where(x => (x.idUser == iduser || x.idUserRespon == iduser) && x.idTimefulfil == vstatus.idTimefulfil).ToList();

            return ltask;
        }

        //Mostrar el listado de tareas del usuario que tiene asignado  
        public List<Countask> countstask(int id)
        {
            int valor;
            List<Countask> counters = new List<Countask>();
            List<typeFulfillment> stats = db.typeFulfillments.ToList();
            int y = 1;

            foreach(var item in stats)
            {
                counters.Add(new Countask {
                    idtask= item.idTimefulfil,
                    counter = db.tasks.Count(x => x.idTimefulfil == item.idTimefulfil && (x.idUser == id || x.idUserRespon == id))
                });
            }
            
            return counters.ToList();
        }

        //****************************************************************************//
        //Actualizar los status de la informacion automaticamente
        public void Modificltask()
        {
            Mail mail = new Mail();
            List<task> ltask = db.tasks.Where(x => x.idTimefulfil == 3 && x.idTimefulfil == 4 && x.idTimefulfil == 5).ToList();
            List<typeFulfillment> lstatus = new List<typeFulfillment>();
            int num = Convert.ToInt32(DateTime.Now.Day);
            DateTime now = DateTime.UtcNow;

            foreach (var item in ltask)
            {
                TimeSpan ts = Convert.ToDateTime(item.dDelivery) - now;
                typeFulfillment type = db.typeFulfillments.Where(x => x.dayfinish > 0 && x.dayfinish < Math.Abs(ts.Days)).FirstOrDefault();

                if (type == null)
                {
                    item.idTimefulfil = 5;
                    db.SaveChanges();

                    string message = mail.enviar(Convert.ToInt32(item.idUser), Convert.ToInt32(item.idUserRespon), Convert.ToInt32(item.idTimefulfil), item.Idtask);
                }
                else
                {
                    if (type.idTimefulfil == 3)
                    {
                        item.idTimefulfil = 3;
                        db.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(item.idUser), Convert.ToInt32(item.idUserRespon), Convert.ToInt32(item.idTimefulfil), item.Idtask);

                    }
                    else
                    if (type.idTimefulfil == 4)
                    {
                        item.idTimefulfil = 3;
                        db.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(item.idUser), Convert.ToInt32(item.idUserRespon), Convert.ToInt32(item.idTimefulfil), item.Idtask);
                    }
                    else if (type.idTimefulfil == 5)
                    {
                        item.idTimefulfil = 4;
                        db.SaveChanges();

                        string message = mail.enviar(Convert.ToInt32(item.idUser), Convert.ToInt32(item.idUserRespon), 2, item.Idtask);
                    }
                }
            }
        }
    }
}