﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace SicoinInets.Utilities
{
	public class Crypto
	{

		public string Encript(string inputText)
		{
			SHA1CryptoServiceProvider cryp = new SHA1CryptoServiceProvider();
			byte[] vecbytes = System.Text.Encoding.UTF8.GetBytes(inputText);
			byte[] inarray = cryp.ComputeHash(vecbytes);

			cryp.Clear();
			return Convert.ToBase64String(inarray);
		}
	}
}