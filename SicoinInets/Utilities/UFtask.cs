﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SicoinInets.Models;
using SicoinInets.Helper;


namespace SicoinInets.Utilities
{
    public class UFtask:ExcelReader_
    {
        int rows = 0;

        public static readonly string[] Header = {"Autoridad", "Tarea","Id", "Dep", "Portal", "Responsable", "Fundamento", "Periodo", "Jefe Inmediato", "F. Entrega"};
        public static readonly string[] Headerspaces = { "Autoridad", "Tarea", "Id", "Dep", "Portal", "Responsable", "Fundamento", "Periodo", "Jefe Inmediato", "F. Entrega" };

        string[] patterColumns = {
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,100}$" ,// Autoridad
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡\%]{1,200}$" ,// Tarea
            @"^[0-9]{1,8}$",//Id
            @"^[0-9]{1,8}$",//Dep
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,200}$" ,// Portal
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,200}$",// Responsable
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,200}$" ,// Fundamento
            @"^[a-zA-ZñÑáÁéÉíÍóÓúÚäÄëËïÏöÖüÜ0-9\(\)\´\,\:\.\-\/\s\&\#\!\¡]{1,200}$",// Periodo
            @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$",// Jefe inmediato
            @"(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])\/(0?[1-9]|1[0-2])\/([2]{1}[0-9]{1}[0-9]{1}[0-9]{1})"//F. Entrega
        };

        string[] msnpattern = {
            "¡Alerta! El formato para la columna Autoridad solo permite letras y numeros.",//Portal
            "¡Alerta! El formato para la columna Tarea solo permite letras y numeros.",//Portal
            "¡Alerta! El formato para la columna de Id solo permite numeros.",//Id
            "¡Alerta! El formato para la columna de Dep solo permite numeros",//Dep
            "¡Alerta! El formato para la columna Portal solo permite letras y numeros.",//Portal
            "¡Alerta! El formato para la columna Responsable solo permite letras y numeros.",//Responsable
            "¡Alerta! El formato para la columna Fundamento solo permite letras y numeros.",//Fundamento
            "¡Alerta! El formato para la columna Solo permite una letra.",//Periodo
            "¡Alerta! El formato para la columna Jefe inmediato no cumple con los caracteres de un correo electronico.",//Jefe Inmediato
            "¡Alerta! El formato para la columna F. entrega no cumple con los caracteres de una fecha.",//F. entrega
        };

        public (List<TmpTask>, List<string>) Loadftask()
        {
            ResponseWrapper errors = new ResponseWrapper();
            List<TmpTask> tmps = new List<TmpTask>();
            ResponseWrapper error = new ResponseWrapper();
            if (rw > 0)
            {
                //ERRORES
                for (rCnt = 1; rCnt < rw; rCnt++)
                {
                    error = new ResponseWrapper();

                    var row = table.Rows[rCnt];
                    List<string> pila_;
                    //Autority
                    error.AppendRangeErrors(
                        Data_.IsNull(
                            row[0].ToString(),
                            msnpattern[0],
                            patterColumns[0],
                            rCnt
                            ));

                    //Task
                    error.AppendRangeErrors(
                        Data_.IsNull(
                            row[1].ToString(),
                            msnpattern[1],
                            patterColumns[1],
                            rCnt
                            ));

                    //Id
                    error.AppendRangeErrors(
                        Data_.IsNull(
                            row[2].ToString(),
                            msnpattern[2],
                            patterColumns[2],
                            rCnt
                            ));

                    //Dep
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[3].ToString(),
                            msnpattern[3],
                            patterColumns[3],
                            rCnt
                            ));

                    //Portal
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[4].ToString(),
                            msnpattern[4],
                            patterColumns[4],
                            rCnt
                            ));

                    //Responsable
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[5].ToString(),
                            msnpattern[5],
                            patterColumns[5],
                            rCnt
                            ));

                    //Fundamento
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[6].ToString(),
                            msnpattern[6],
                            patterColumns[6],
                            rCnt
                            ));

                    //% Periodo
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[7].ToString(),
                            msnpattern[7],
                            patterColumns[7],
                            rCnt
                            ));

                    //Jefe Inmediato 
                    error.AppendRangeErrors(
                        Data_.IsEmpty_(
                            row[8].ToString(),
                            msnpattern[8],
                            patterColumns[8],
                            rCnt
                            ));

                    //F. entrega
                    error.AppendRangeErrors(
                        Data_.IsNull(
                            row[9].ToString(),
                            msnpattern[9],
                            patterColumns[9],
                            rCnt
                            ));

                    if (error.Errors.Count <= 0)
                    {
                        tmps.Add(new TmpTask()
                        {
                            Autority = row[0].ToString().Trim(),
                            Task = row[1].ToString().Trim(),
                            IdTask = row[2].ToString().Trim(),
                            Deptask = row[3].ToString().Trim(),
                            Portal = row[4].ToString().Trim(),
                            Responsability = row[5].ToString().Trim(),
                            Fundamento = row[6].ToString().Trim(),
                            tPeriod = row[7].ToString().Trim(),
                            BossInmediato = row[8].ToString().Trim(),
                            DateDelivery = row[9].ToString().Trim()
                        });
                    }
                    else
                    {
                        errors.AppendRangeErrors(error.Errors);
                        error = null;
                    }
                }
                
                bulk.inserttemp(tmps);

                List<string> result = validate();

                if (result.Count() > 0)
                {
                    errors.AppendRangeErrors(result);
                }

            }
            else
            {
                errors.AddError(string.Format("¡Alerta! Archivo vacio."));
            }
            return (tmps, errors.Errors);
        }

        public static List<string> validate()
        {
            List<string> result = new List<string>();
            using (var ps = new DbSicoinEntities1())
            {
                var profile = ps.getProfile().ToList();
                if (profile.Count() != 0)
                {
                    result.Add("ESTOS PERFILES NO HAN SIDO REGISTRADOS EN LA BASE DE DATOS ");
                    foreach (var item in ps.getProfile().ToList())
                    {
                        result.Add(item.ToString());
                    }
                }

                var user = ps.getUser().ToList();
                if (user.Count() != 0)
                {
                    result.Add("ESTOS PERFILES NO HAN SIDO ASIGNADO A NINGUN USUARIO ");
                    foreach (var item in ps.getProfile().ToList())
                    {
                        result.Add(item.ToString());
                    }
                }
            }

            return result.ToList();
        }
    }
}