﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace SicoinInets.Utilities
{
    public class Data_
    {
        private static string[] months = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

        public static string GetMonthForNumber(int number)
        {
            return months[number - 1];
        }

        public static String GetDateNow()
        {
            return DateTimeZone_.Now().ToString("ddMMyyyyHHmmss");
        }

        public static DateTime GetDateddmmyy(int month, string year)
        {
            string smonth = "";
            if (month < 10)
            {
                smonth = String.Format("0{0}", month);
            }
            else
            {
                smonth += month;
            }
            string dte = String.Format("01/{0}/{1}", smonth, year);
            DateTime date = DateTime.ParseExact(dte, "dd/MM/yyyy", null);
            return date;
        }

        public static DateTime GetDateddmmyy(int month, int year)
        {
            string smonth = "";
            if (month < 10)
            {
                smonth = String.Format("0{0}", month);
            }
            else
            {
                smonth += month;
            }
            string dte = String.Format("01/{0}/{1}", smonth, year);
            DateTime date = DateTime.ParseExact(dte, "dd/MM/yyyy", null);
            return date;

        }

        public static string GetDateWithoutformat(int month, string year)
        {
            string smonth = "";
            if (month < 10)
            {
                smonth = String.Format("0{0}", month);
            }
            else
            {
                smonth += month;
            }
            string dte = String.Format("01{0}{1}", smonth, year);

            return dte;
        }

        public static (List<String>, decimal) IsEmptyAndDecimal_(string cell, string msnpattern, int numline, string pattern)
        {
            ResponseWrapper error = new ResponseWrapper();
            decimal val = 0;
            var msn = "Línea " + (numline + 1) + ": " + msnpattern;
            //if (cell == "")
            //{
            //    error.AddError(string.Format(msn, cell, (numline + 1)));
            //}
            //else
            //{
            var v = MatchData(pattern, cell, msn);
            if (v.Count > 0)
            {
                error.AppendRangeErrors(v);
            }
            else
            {
                if (Decimal.TryParse(cell, out val) == false)
                {
                    val = 0;
                }
            }
            //Decimal.TryParse(cell, out val);
            //if (Decimal.TryParse(cell, out val) == false)
            //{
            //    var v = MatchData(pattern, cell, "Linea " + (numline + 1)+": "+msnpattern);
            //    error.AppendRangeErrors( v);
            //}
            //}
            return (error.Errors, val);
        }

        public static (List<String>, int) IsEmptyAndInt(string cell, string msnempty, string msntype, int numline)
        {
            ResponseWrapper error = new ResponseWrapper();
            int val = 0;
            if (cell == "")
            {
                error.AddError(string.Format(msnempty, cell, (numline + 1)));
            }
            else
            {
                if (int.TryParse(cell, out val) == false)
                {
                    error.AddError(String.Format(msntype, cell, (numline + 1)));
                }
            }
            return (error.Errors, val);
        }

        public static List<String> IsEmpty_(string cell, string msnpattern, string pattern, int numline)
        {
            ResponseWrapper error = new ResponseWrapper();
            cell = cell.Replace(" ", String.Empty);
            var match = MatchData(pattern, cell, String.Format("Línea {0}: {1} ", (numline + 1), msnpattern));
            if (match.Count() > 0)
            {
                error.AppendRangeErrors(match);
            }
            return error.Errors;

        }

        public static String GetToStringList(String[] header)
        {
            string cad = "";
            foreach (var item in header)
            {
                cad += item + ", ";
            }
            return cad;
        }

        public static List<string> MatchData(string pattern, string input, string msnpattern)
        {
            ResponseWrapper error = new ResponseWrapper();
            Match m = Regex.Match(input, pattern, RegexOptions.IgnoreCase);
            if (m.Success == false)
            {
                error.AddError(msnpattern);
            }
            return error.Errors;
        }

        public static string EncodePassword(string Contraseña)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(Contraseña);
            byte[] hash = sha1.ComputeHash(inputBytes);
            return Convert.ToBase64String(hash);
        }

        public static List<String> IsNull(string cell, string msnpattern, string pattern, int numline)
        {
            ResponseWrapper error = new ResponseWrapper();

            if (cell != null && cell != "")
            {
                cell = cell.Replace(" ", String.Empty);
                var match = MatchData(pattern, cell, String.Format("Línea {0}: {1} ", (numline + 1), msnpattern));

                if (match.Count() > 0)
                {
                    error.AppendRangeErrors(match);
                }
            }

            return error.Errors;
        }

    }
}