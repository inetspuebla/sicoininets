﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicoinInets.Utilities
{
    public class Alert
    {

        public static readonly string noti_danger = "danger";
        public static readonly string noti_success = "success";
        public static readonly string noti_warning = "warning";
        #region properties_alerts
        public static readonly string None = "alert";
        public static readonly string Default = "alert alert-default";
        public static readonly string Primary = "alert alert-primary";
        public static readonly string Info = "alert alert-info";
        #endregion

        #region propities_notify
        public static readonly string Success = "alert alert-success";
        public static readonly string Warning = "alert alert-warning";
        public static readonly string Danger = "alert alert-danger";
        #endregion

        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool Dismissible { get; set; }

        public Alert()
        {
            this.Title = string.Empty;
            this.Description = string.Empty;
            this.Type = Alert.None;
            this.Dismissible = true;
        }

        public Alert(string title, string description, bool dismiss = true)
        {
            this.Title = title;
            this.Description = description;
            this.Type = Alert.None;
            this.Dismissible = dismiss;
        }

        public Alert(string title, string description, string type, bool dismiss = true)
        {
            this.Title = title;
            this.Description = description;
            this.Type = type;
            this.Dismissible = dismiss;
        }
    }
}