﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using ExcelDataReader;

namespace SicoinInets.Utilities
{
    public class ExcelReader_
    {
        protected int rCnt;// contador de filas
        protected int cCnt;// contador de columnas

        protected int rw;// numero de filas
        protected int cl; // numero de columnas
        protected IExcelDataReader reader = null;
        protected DataTable table;

        public (int, string) CompareNumberColumnsFileWithStructHeader(FileStream stream, string[] header)
        {
            try
            {
                using (reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();
                    table = result.Tables[0];
                    rw = table.Rows.Count;
                    cl = table.Columns.Count;
                    if (rw == 0 || cl == 0)
                    {
                        return (-1, "No hay datos, archivo vacio.");// archivo vacio
                    }
                    if (cl == header.Count())
                    {
                        return (0, "");
                    }
                    return (1, "");// Las columnas no empatan
                }
            }
            catch (Exception e)
            {
                return (-1, e.Message);
            }
        }
        public string[] GetHeaderFile()
        {
            string[] header = new string[cl];
            var row = table.Rows[0];
            for (int i = 0; i < cl; i++)
            {
                header[i] = row[i].ToString();
            }
            return header;
        }
        public (bool, List<string>) CompareColumnsFileWithStructHeader(string filePathSource, string[] header)
        {
            ResponseWrapper errorsheaders = new ResponseWrapper();
            var row = table.Rows[0];
            for (int i = 0; i < cl; i++)
            {
                var val = row[i].ToString().ToLower().Replace(" ", String.Empty);
                if (val != header[i].ToLower())
                {
                    errorsheaders.AddError(string.Format("¡Alerta! Nombre de columna {0} no corresponde con la columna {1}.", row[i].ToString(), header[i]));
                }
            }
            if (errorsheaders.Errors.Count > 0)
            {
                return (false, errorsheaders.Errors);
            }
            return (true, null);
        }

        public (bool, List<string>) CompareColumnsFileWithStructHeader(string[] header)
        {
            ResponseWrapper errorsheaders = new ResponseWrapper();
            var row = table.Rows[0];
            for (int i = 0; i < cl; i++)
            {
                var val = row[i].ToString().ToLower().Replace(" ", String.Empty);
                if (val != header[i].ToLower().Replace(" ", String.Empty))
                {
                    errorsheaders.AddError(string.Format("¡Alerta! Nombre de columna {0} del documento no corresponde con la columna {1} definida.", row[i].ToString(), header[i]));
                }
            }
            if (errorsheaders.Errors.Count > 0)
            {
                return (false, errorsheaders.Errors);
            }
            return (true, new List<String>());
        }
    }
}