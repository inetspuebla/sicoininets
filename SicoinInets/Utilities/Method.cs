﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicoinInets.Utilities
{
	public class Method
	{
		public string typedown(string ext)
		{
			string result = "";

			switch(ext)
			{
				case ".xlsx":
					result = "application/vnd.ms-excel";
					break;
				case ".pdf":
					result = "application/pdf";
					break;
				case ".pptx":
					result = "application/vnd.ms-powerpoint";
					break;
				case ".docx":
					result = "application/msword";
					break;
                case ".jpg":
                    result = "image/jpeg";
                    break;
                case ".png":
                    result = "image/png";
                    break;
                case ".vsd":
                    result = "application/vnd.visio";
                    break;
                case ".mpp":
                    result = "application/vnd.ms-project";
                    break;

            }
			
			return result;
		}

	}
}