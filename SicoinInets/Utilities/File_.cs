﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SicoinInets.Utilities
{
    public static class File_
    {
        public static bool ExistFile(string namefilesource, string folder)
        {
            var filePathSource = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/{0}/", folder)), namefilesource);
            return File.Exists(filePathSource);
        }
        public static string CheckSizeFile(int size)
        {
            if (size > 1010000)
            {
                return "El archico es demasiado grande. Se aceptan archivos no mayores a 1 MB.";
            }
            return "";
        }
        public static bool CheckExtension(string namefilesource, string folder)
        {
            var filePathSource = HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folder)) + namefilesource;
            //string pathfile = pathfile + namefilesource;
            var st = Path.GetExtension(filePathSource);
            if (Path.GetExtension(filePathSource) != ".xlsx" && Path.GetExtension(filePathSource) != ".xls")
            {
                return false;
            }
            return true;
        }
        public static string GetPathFile(string namefilesource, string folder)
        {
            var filePathSource = HttpContext.Current.Server.MapPath(String.Format("~/{0}/", folder)) + namefilesource;
            //string filePathSource = pathcsv + namefilesource;
            return filePathSource;
        }
        public static bool SaveFile(HttpPostedFileBase file, string folder)
        {
            try
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/{0}/", folder)), fileName);
                file.SaveAs(path);
                return true;
            }
            catch
            {
                return false;
            }


        }

        public static (bool, string) SaveFile(HttpPostedFileBase file, string folder, int i = 0)
        {
            try
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/{0}/", folder)), fileName);
                file.SaveAs(path);
                return (true, "");
            }
            catch (Exception e)
            {
                return (false, e.Message);
            }


        }

        internal static bool ExistFile(string namefilesource, object folder)
        {
            throw new NotImplementedException();
        }

        public static (bool, string, string) SaveFile(HttpPostedFileBase file, string folder, string newnamefile)
        {
            try
            {
                var extension = Path.GetExtension(file.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/{0}/", folder)), newnamefile + extension);
                file.SaveAs(path);
                return (true, "", path);
            }
            catch (Exception e)
            {
                return (false, e.Message, "");
            }
        }

        public static (string, FileStream) OpenFile(string folder, string namefile)
        {
            string path = namefile;
            try
            {
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return ("", stream);
            }
            catch (Exception e)
            {
                return (e.Message, null);
            }
        }
        //***
        public static (string, FileStream) OpenFile(string namefile)
        {
            string path = namefile;//GetPathFile(namefile, folder);
            try
            {
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return ("", stream);
            }
            catch (Exception e)
            {
                return (e.Message, null);
            }
        }
        public static void CloseFile(FileStream stream)
        {
            stream.Close();
            //System.IO.File.Delete(HttpContext.Current.Server.MapPath(string.Format("~/{0}/{1}", directorio, uuid)));
        }

        public static void CloseFile(FileStream stream, string pathandnamefile)
        {
            stream.Close();
            System.IO.File.Delete(pathandnamefile);
        }

    }
}