﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SicoinInets.Models;

namespace Sicoin.Controllers
{
    public class UserSetsController : Controller
    {
        
        private DbSicoinEntities1 db = new DbSicoinEntities1();

        // GET: UserSets
        public ActionResult Index()
        {
            var userSet = db.UserSets.ToList();
            return View(userSet.ToList());
        }

        // GET: UserSets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSet userSet = db.UserSets.Find(id);
            if (userSet == null)
            {
                return HttpNotFound();
            }
            return View(userSet);
        }

        // GET: UserSets/Create
        public ActionResult Create()
        {
            ViewBag.IdProfile = new SelectList(db.ProfileSets, "IdProfile", "sName");
            return View();
        }

        // POST: UserSets/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdUser,sName,sMail,IdProfile,IdPosition")] UserSet userSet)
        {
            if (ModelState.IsValid)
            {
                db.UserSets.Add(userSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdProfile = new SelectList(db.ProfileSets, "IdProfile", "sName", userSet.IdProfile);
            return View(userSet);
        }

        // GET: UserSets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSet userSet = db.UserSets.Find(id);
            if (userSet == null)
            {
                return HttpNotFound();
            }

            ViewBag.IdProfile = new SelectList(db.ProfileSets, "IdProfile", "sName", userSet.IdProfile);
            return View(userSet);
        }

        // POST: UserSets/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdUser,sName,sMail,IdProfile,IdPosition")] UserSet userSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdProfile = new SelectList(db.ProfileSets, "IdProfile", "sName", userSet.IdProfile);
            return View(userSet);
        }

        // GET: UserSets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSet userSet = db.UserSets.Find(id);
            if (userSet == null)
            {
                return HttpNotFound();
            }
            return View(userSet);
        }

        // POST: UserSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSet userSet = db.UserSets.Find(id);
            db.UserSets.Remove(userSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
