﻿function showModalstamp() {
    var schema = $(`input[name='schema']`).val();
    var year = $(`input[name='year']`).val();
    var month = $(`input[name='month']`).val();
    $("#modal_stamp").modal("show");
}
function aceptStamp() {
    var schema = $(`input[name='schema']`).val();
    var year = $(`input[name='year']`).val();
    var month = $(`input[name='month']`).val();
    $("#modal_login_stamp").modal("show");
}
$(function () {
    $.validator.addMethod("PatternTest", function (value, element, arg) {
        var patt = new RegExp(arg);
        return patt.test(value);
    }, "incorrecto.");
    $("#formpermmissions").validate({
        submitHandler: function (form) {
            $(form).find('button[type=submit]').prop('disabled', true);
            //console.log($())
            $(form).ajaxSubmit();
        },
        rules: {
            keyWorker: {
                required: true,
                PatternTest: /^[0-9]{1,8}$/,
                maxlength: 8,
                minlength: 1
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 30,
            }
        },
        messages: {
            keyWorker: {
                required: " Clave de trabajador requerido.",
                PatternTest: "Acepta sólo números.",
                maxlength: "Acepta hasta 8 números.",
                minlength: "Acepta hasta 8 números."
            },
            password: {
                required: "Contraseña requerida.",
                minlength: "Acepta 8 caracteres mínimos.",
                maxlength: "Acepta hasta 30 caracteres.",
            }
        }
    });
});