﻿
var Success = "alert alert-success";
var Warning = "alert alert-warning";
var Danger = "alert alert-danger";

class Notice {

    showNotify( type, msn) {
        $("#_notify").show();
        $("#_notify").attr("class", "");
        $("#_notify").addClass(type);
        $("#errors").html(msn);
    }
    hideNotify() {
        $("#_notify").attr("class", "");
        $("#_notify").hide();
    }
    showErrors(errors, numbererrors) {
        $("#_msns").show();
        $("#_msn").html( errors );
        $("#numerrors").html( numbererrors );
    }
    hideErrors() {
        $("#_msns").hide();
        $("#_msn").html("");
        $("#numerrors").html("");
    }
}