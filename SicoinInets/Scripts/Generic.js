﻿function Alerts() {
    $(".notify").delay(5090).fadeOut(5090);
}
function CheckextensionFile(files) {
    console.log(files);
    var name = files[0].name;
    var size = files[0].size; // bytes
    var pattern = ".xls[x]?$";
    if (name.match(pattern) && size < 4000000) { // 4MB
        return 1;
    } else {
        return -1;
    }
}
let Months = new Array();
Months["Enero"] = 1;
Months["Febrero"] = 2;
Months["Marzo"] = 3;
Months["Abril"] = 4;
Months["Mayo"] = 5;
Months["Junio"] = 6;
Months["Julio"] = 7;
Months["Agosto"] = 8;
Months["Septiembre"] = 9;
Months["Octubre"] = 10;
Months["Noviembre"] = 11;
Months["Diciembre"] = 12;

function validateErrorJS(status) {
    switch (status) {
        case 401:
            alert("Se ha terminado su sesión. Inicie sesión.");
            setTimeout(function () {
                window.location.href = "/LogIn/Index";
            }, 300);
            break;
        case 403:
            f_message("<b>¡No permitido!</b> No tiene permisos para hacer esta operación.", "danger")
            break;
        case 505:
            f_message("<b>¡Error!</b> Sucedio un error en la operación. Cargue nuevamente la página.", "danger");
            break;
        default:
            f_message("<b>¡Error!</b> Sucedio un error en la operación. Cargue nuevamente la página.", "danger");
            break;
    }
}

function f_message(message, typenotify) {
    $.notify({
        message: message
    },
        { type: typenotify });
}

function SaveAjaxLoadDataFile(url, data, nameform) {
    var boton = new Boton();
    var notice = new Notice();
    boton.disabled(nameform);
    $.ajax({
        url: url,
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            $("#loader").show();
        },
        success: function (data) {
            $("#loader").hide();
            boton.enable(nameform);

            if (data._notify) {
                notice.hideErrors();
                notice.showNotify(data._notify.Type, `<strong>${data._notify.Title}</strong>${data._notify.Description}`);
            } else {
                notice.hideNotify();
            }
            if (data._msn) {
                notice.hideNotify();
                notice.showErrors(
                    `<textarea class='form-control' rows="23" readonly>${data._msn}</textarea>`,
                    `<p class="pull-right">No. Errores: ${data.cont}</p>`);
            } else {
                notice.hideErrors();
            }
        },
        error: function (XHR, ajaxOption, ThrowError) {
            $("#loader").hide();
            boton.enable(nameform);
            validateErrorJS(XHR.status);
        }
    });
}
function InitDataPicker(nameinput) {
    $(nameinput).datepicker(
        {
            closeText: 'Aceptar',
            appendText: "",
            buttonText: "",
            prevtext: '<Ant', nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function (dateText, inst) { $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1)) }
        });
    $.datepicker.setDefaults($.datepicker.regional['es']);
}

$.validator.addMethod("valueNotEquals", function (value, element, arg) {
    return arg !== value;
}, "Value must not equal arg.");

$.validator.addMethod("PatternTest", function (value, element, arg) {
    var patt = new RegExp(arg);
    console.log("Entre a patterntest");
    return patt.test(value);
}, "incorrecto.");

function Checksize(files) {
    console.log(files);
    var name = files[0].name;
    var size = files[0].size; // bytes
    console.log(size);


    if (size < 5000000) { // 2MB
        return 1;
    } else {
        return -1;
    }
}