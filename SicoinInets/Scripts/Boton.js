﻿class Boton {
    disabled(id) {
        var $form = $(id);
        var $btnSubmit = $form.find('button[type=submit]');
        $btnSubmit.prop('disabled', true);
        $btnSubmit.html('<i>Espere...</i>');
    }
    enable(id) {
        var $form = $(id);
        //$form.trigger("reset");
        var $btnSubmit = $form.find('button[type=submit]');
        $btnSubmit.prop('disabled', false);
        $btnSubmit.html(`Guardar <i class="glyphicon glyphicon-ok"><i>`);
    }
}