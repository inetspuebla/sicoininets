﻿$(function () {
    $(".detalles").on("click", function () {
        $("#detailscommissions").modal("show");
        var daywork = $(this).data("daywork");
        var anticipation = $(this).data("anticipation");
        var visit = $(this).data("visit");
        var worker = $(this).data("worker");
        var keyworker = $(this).data("keyworker");
        $("#contentdetails").html(
            `<div><label>${keyworker} - ${worker}</label></div>
                        <div><label> Días trabajados:  </label>  ${daywork}</div>
                        <div><label> Anticipación:  </label>  ${anticipation}</div>
                        <div><label> Factor de visita:</label>  ${visit}</div>`
        );
    })

    $(".detallescoord").on("click", function () {
        $("#detailscommissions").modal("show");
        var factzone = $(this).data("factzone");
        var dayworker = $(this).data("dayworker");
        var fulfillment = $(this).data("fulfillment");
        var goalinventary = $(this).data("goalinventary");
        var worker = $(this).data("worker");
        var keyworker = $(this).data("keyworker");
        $("#contentdetails").html(
            `<div><label>${keyworker} - ${worker}</label></div>
            <div><label> Factor de zona:  </label>  ${factzone}</div>
            <div><label> Días trabajados:  </label>  ${dayworker}</div>
            <div><label> % de cumplimiento:</label>  ${fulfillment}</div>
            <div><label> Obj. de inventario:</label>  ${goalinventary}</div>`
        );
    });
});