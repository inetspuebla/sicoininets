﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SicoinInets.Models;
using SicoinInets.Sends;
using SicoinInets.Utilities;

namespace SicoinInets.Bussines
{
    public class bTask
    {
        public static List<string> Savetask(List<TmpTask> tasks, int iduser)
        {
            DbSicoinEntities1 db = new DbSicoinEntities1();
            task task = new task();
            ProfileSet prof = new ProfileSet();
            UserSet user = new UserSet();
            ResponseWrapper errors = new ResponseWrapper();
            Mail mail = new Mail();
            string message="";

            DateTime now = DateTime.UtcNow;

            List<TmpTask> tmp = db.TmpTasks.ToList();
            foreach (var item in tmp)
            {
                prof = db.ProfileSets.Where(x => x.sName == item.Responsability).FirstOrDefault();
                user = db.UserSets.Where(x => x.IdProfile == prof.IdProfile).FirstOrDefault();
                
                task.sNameTask = item.Task;
                task.idUser = Convert.ToInt32(user.IdUser);
                task.idUserRespon = iduser;
                task.idTimefulfil = 1;
                task.Priority = item.tPeriod;
                task.Description = item.Fundamento;
                task.DateEmision = now;
                task.emailBoss = item.BossInmediato;
                task.dDelivery = Convert.ToDateTime(item.DateDelivery);
                task.Portal = item.Portal;
                    
                db.tasks.Add(task);
                db.SaveChanges();

                task taskemail = db.tasks.Where(x => x.sNameTask == item.Task).FirstOrDefault();
                 message = mail.enviar(Convert.ToInt32(taskemail.idUser), Convert.ToInt32(taskemail.idUserRespon), Convert.ToInt32(taskemail.idTimefulfil), taskemail.Idtask);

            }

            errors.Errors.Add(message);

            return errors.Errors;
        }
    }
}