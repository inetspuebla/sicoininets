﻿using System.Web;
using System.Web.Optimization;

namespace SicoinInets
{
	public class BundleConfig
	{
		// Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js",
                       "~/Scripts/bootstrap-notify.js",
                       "~/Scripts/sb-admin-2.js",
                       "~/Scripts/metisMenu.js",
                       "~/Scripts/datatables.js",
                       "~/Scripts/Notice.js",
                       "~/Scripts/Boton.js",
                       "~/Scripts/Generic.js",
                       "~/Scripts/jquery-ui.js",
                       "~/Scripts/jquery.validate.js",
                       "~/Scripts/select2.min.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.validate*"));

			// Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
			// para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js")
                      );
            bundles.Add(new StyleBundle("~/Content/css").Include(
                         "~/Content/bootstrap.css",
                         "~/Content/sb-admin-2.css",
                         "~/Content/font-awesome.css",
                         "~/Content/datatables.css",
                         "~/Content/jquery-ui.css",
                         "~/Content/select2.min.css"
                         ));

        }
	}
}
